<?php
class AdminController extends BaseController{

    public function __construct($request){
        parent::__construct($request);
    }


    public function panelAction(){
        if (App::isAjaxRequest()) {
            $response = array(
                'div'=>$this->loadPage('admin/panel',array('data'=>null),true),
                'success'=>true
            );
            echo json_encode($response);
            exit;
        }else{
            $this->loadPage('admin/panel',array('data'=>null));
        }
    }

    public function userAction(){
        if (App::isAjaxRequest()) {
            $response = array(
                'div'=>$this->loadPage('user/create',array('data'=>null),true),
                'success'=>true
            );
            echo json_encode($response);
            exit;
        }else{
            $this->loadPage('user/create',array('data'=>null));
        }
    }
    public function folderAction(){
        if (App::isAjaxRequest()) {
            $response = array(
                'div'=>$this->loadPage('folder/create',array('data'=>null),true),
                'success'=>true
            );
            echo json_encode($response);
            exit;
        }else{
            $this->loadPage('folder/create',array('data'=>null));
        }
    }


}


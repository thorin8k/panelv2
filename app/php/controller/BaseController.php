<?php
class BaseController{
    public $actualPath='';
    
    public function __construct($request){
        $this->actualPath = $request;
    }
    
    /**
     * Se encarga de cargar una vista y adjuntarle siempre la instancia del controlador
     * desde el cual es llamado.
     * 
     * @param String $name
     * @param Array $data
     * @param Boolean $return
     */
    public function loadPage($name,$data=array(),$return = false){
        //Send controller to view
        if(!is_array($data)) $data = array();
        $data =  array_merge($data,array('controller'=>$this));
        return App::loadPage($name,$data,$return);
    }
}
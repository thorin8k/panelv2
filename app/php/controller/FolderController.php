<?php
class FolderController extends BaseController{

    public function createAction(){
        if(App::isAjaxRequest()){
            $model = new MusicFolder();

            if(isset($_POST['MusicFolder'])){
                $attributes = $_POST['MusicFolder'];
                $model->set_attributes($attributes);
                $model->save();
                //TODO save on ldap

                $response = array(
                    'div'=> 'Guardado correctamente',
                    'form'=>false,
                    'success'=>true
                );
                echo json_encode($response);
                exit;

            }else{
                $response = array(
                    'div'=> $this->loadPage('folder/_form', array('model'=>$model), true),
                    'form'=>true,
                    'success'=>true
                );
                echo json_encode($response);
                exit;
            }
        }
    }

    public function updateAction(){
        if(App::isAjaxRequest()){
            $uId = filter_input(INPUT_GET, 'id');

            $folderDao = new FolderDao();
            $model = $folderDao->getByPk($uId);
            if(isset($_POST['MusicFolder'])){
                $attributes = $_POST['MusicFolder'];
                $model->update_attributes($attributes);
                //TODO update on ldap
                $response = array(
                    'div'=> 'Guardado correctamente',
                    'form'=> false,
                    'success'=>true
                );
                echo json_encode($response);
                exit;

            }else{
                $response = array(
                    'div'=> $this->loadPage('folder/_form', array('model'=> $model), true),
                    'form'=> true,
                    'success'=>true
                );
                echo json_encode($response);
                exit;
            }
        }
    }

    public function deleteAction(){
        if(App::isAjaxRequest()){
            $uId = filter_input(INPUT_GET, 'id');
            $folderDao = new FolderDao();
            $model = $folderDao->getByPk($uId);
            $model->deleted = 1;
            $model->save();
            $response = array(
                'div'=> 'Borrado correctamente',
                'success'=>true
            );
            echo json_encode($response);
            exit;
        }
    }
}


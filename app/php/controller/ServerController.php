<?php
class ServerController extends BaseController{
    
    public function __construct($request){
        parent::__construct($request);
    }
    
    public function panelAction(){
        if (App::isAjaxRequest()) {
            $response = array(
                'div'=>$this->loadPage('server/panel',array('data'=>null),true),
                'success'=>true
            );
            echo json_encode($response);
            exit;
        }else{
            $this->loadPage('server/panel',array('data'=>null));
        }
    }
    public function infoAction(){
        if (App::isAjaxRequest()) {
            $response = array(
                'div'=>$this->loadPage('server/info',array('data'=>null),true),
                'success'=>true
            );
            echo json_encode($response);
            exit;
        }else{
            $this->loadPage('server/info',array('data'=>null));
        }
    }
    public function nasinfoAction(){
        if (App::isAjaxRequest()) {
            $response = array(
                'div'=>$this->loadPage('server/nasinfo',array('data'=>null),true),
                'success'=>true
            );
            echo json_encode($response);
            exit;
        }else{
            $this->loadPage('server/nasinfo',array('data'=>null));
        }
    }
    
    public function mcAction(){
        if (App::isAjaxRequest()) {
            $response = array(
                'div'=>'Todavia no esta hecho.',
                'success'=>true
            );
            echo json_encode($response);
            exit;
        }else{
            //$this->loadPage('folder/create',array('data'=>null));
        }
    }
}

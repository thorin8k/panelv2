<?php
class SiteController extends BaseController{

    function __construct($request){
        parent::__construct($request);
    }

    public function indexAction(){
        $test = 'test';
        //After do something show the page passing the data...
        if (App::isAjaxRequest()) {
            $response = array(
                'div'=>$this->loadPage('site/index',array('data'=>$test),true),
                'success'=>true
            );
            echo json_encode($response);
            exit;
        }else{
            $this->loadPage('site/index',array('data'=>$test));
        }
    }

    public function errorAction($params){

        $this->loadPage('site/error',$params);
    }

    public function loginAction(){
            if (App::isAjaxRequest()) {
            $user = filter_input(INPUT_POST, 'login');
            $password = filter_input(INPUT_POST, 'password');
            $response = array(
                    'message'=>'Usuario o contraseña incorrectos',
                    'div'=>  AppUtils::getLoginForm(true),
                    'reload' => false,
                    'success'=>false
            );
            if(App::getSession()->login($user,$password)){
                $response = array(
                    'div'=>AppUtils::getLoggedText($user),
                    'reload' => true,
                    'success'=>true
                );
            }
            echo json_encode($response);
            exit;
        }
    }

    public function logoutAction(){
        App::getSession()->logout();
        Header('Location: '.filter_input(INPUT_SERVER,'PHP_SELF'));
    }

    public function testAction(){
        $user = "";
        //$user = User::create(array('login' => 'Test', 'profile' => 'VA'));
        $user = User::first();

        $this->loadPage('site/test',array('user'=>$user));
    }

    /*public function reloadMenuAction(){
        $defMenu = array(
            array('text'=>'Home','page'=>'Home','active' => true),
        );
        $arrayMenu = array_merge($defMenu,App::getSession()->getUserMenu());
        $response = array(
         'div'=>AppUtils::createMenuStruct($arrayMenu),
        );
        echo json_encode($response);
        exit;
    }*/

    public function musicAction(){
        $test = 'test';
        //After do something show the page passing the data...
        if (App::isAjaxRequest()) {
            $response = array(
                'div'=>$this->loadPage('music/browser',array('data'=>$test),true),
                'success'=>true
            );
            echo json_encode($response);
            exit;
        }else{
            $this->loadPage('music/browser',array('data'=>$test));
        }
    }

    public function folderAction(){

        $user = App::getSession()->getUserObject();
        $folder = $user->musicfolders;
        $opts = array();
        foreach ($folder as $value) {
            $opts['roots'][]= array(
                'alias'         => $value->name,
                'driver'        => 'LocalFileSystem',   // driver for accessing file system (REQUIRED)
                'path'          => $value->path,         // path to files (REQUIRED)
                //'URL'           => $value->path, // URL to files (REQUIRED)
                //'accessControl' => 'access'             // disable and hide dot starting files (OPTIONAL)
            );
        }
        $connector = new elFinderConnector(new elFinder($opts));
        $connector->run();
    }

    public function playerAction(){
        $test = 'test';
        if (App::isAjaxRequest()) {
            $response = array(
                'div'=>$this->loadPage('music/player',array('data'=>$test ),true),
                'success'=>true
            );
            echo json_encode($response);
            exit;
        }else{
            $this->loadPage('music/player',array('data'=>$test));
        }
    }
}


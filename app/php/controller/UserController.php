<?php
class UserController extends BaseController{

    public function createAction(){
        if(App::isAjaxRequest()){
            $model = new User();

            if(isset($_POST['User'])){
                $attributes = $_POST['User'];
                $model->set_attributes($attributes);
                $model->save();
                App::getSession()->configureLdap();
                App::getSession()->createUser($attributes['login'], $attributes['password']);
                mkdir($attributes['folder']);
                $folder = new MusicFolder();
                $folder->name = "Musica";
                $folder->path = $attributes['folder'];
                $folder->user_id = $model->id;
                $folder->save();

                $response = array(
                    'div'=> 'Guardado correctamente',
                    'form'=>false,
                    'success'=>true
                );
                echo json_encode($response);
                exit;

            }else{
                $response = array(
                    'div'=> $this->loadPage('user/_form', array('model'=>$model), true),
                    'form'=>true,
                    'success'=>true
                );
                echo json_encode($response);
                exit;
            }
        }
    }

    public function updateAction(){
        if(App::isAjaxRequest()){
            $uId = filter_input(INPUT_GET, 'id');

            $userDao = new UserDao();
            $model = $userDao->getByPk($uId);
            if(isset($_POST['User'])){
                $attributes = $_POST['User'];
                $model->update_attributes($attributes);
                App::getSession()->configureLdap();
                App::getSession()->changePassword($model->login, $attributes['password']);

                $response = array(
                    'div'=> 'Guardado correctamente',
                    'form'=> false,
                    'success'=>true
                );
                echo json_encode($response);
                exit;

            }else{
                $response = array(
                    'div'=> $this->loadPage('user/_form', array('model'=> $model), true),
                    'form'=> true,
                    'success'=>true
                );
                echo json_encode($response);
                exit;
            }
        }
    }

    public function deleteAction(){
        if(App::isAjaxRequest()){
            $uId = filter_input(INPUT_GET, 'id');
            $userDao = new UserDao();
            $model = $userDao->getByPk($uId);
            $model->deleted = 1;
            $model->save();
            //TODO delete from ldap
            $response = array(
                'div'=> 'Borrado correctamente',
                'success'=>true
            );
            echo json_encode($response);
            exit;
        }
    }

    public function profileAction(){
        $model = App::getSession()->getUserObject();
        $response = array(
                'div'=>$this->loadPage('user/profile', array('model'=> $model), true),
                'success'=>true
            );
            echo json_encode($response);
            exit;
    }
    public function changepwdAction(){
        $model = App::getSession()->getUserObject();
        $response = array(
                'div'=>$this->loadPage('user/_pwd_form', array('model'=> $model), true),
                'form'=> true,
                'success'=>true
            );
            echo json_encode($response);
            exit;
    }
}


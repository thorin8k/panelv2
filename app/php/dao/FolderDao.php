<?php
class FolderDao{
    
    public function __construct(){ 
    }
    
    public function getAll(){
        
        return MusicFolder::find('all', array('conditions'=>'deleted = 0'));
    }
    
    public function getByPk($pk){
        return MusicFolder::find((int)$pk);
    }
}


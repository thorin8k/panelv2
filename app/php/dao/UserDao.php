<?php
class UserDao{
    
    public function __construct(){ 
    }
    
    public function getAll(){
        
        return User::find('all', array('conditions'=>'deleted = 0'));
    }
    
    public function getUser($username){
        $user = User::find_by_login($username);
                
        return $user;
    }
    
    public function getByPk($pk){
        return User::find((int)$pk);
    }
}


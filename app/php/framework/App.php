<?php
define('VERSION_FRM','0.1');
define('VERSION_APP','0.1');

define('CONFIG_FILE', 'app/config.json');
define('VIEW_PATH','app/view/');
define('INCLUDES_DIR','app/php/');
define('EXCLUDES_DIR','app/php/lib/excluded/');

define('CLASSLOADER_CLASSPATH','app/php/framework/ClassLoader.php');
define('ACTIVERECORD_CLASSPATH','app/php/lib/excluded/activerecord/ActiveRecord.php');

//Register Autoload classes
include_once CLASSLOADER_CLASSPATH;
spl_autoload_register(array(ClassLoader::getInstance(), 'loadClass'));

/**
 * Clase principal del framework utilizada como wrapper para llamar a los
 * componentes principales. 
 * 
 */
class App{
    //Singleton
    private static $instance;
    
    private $router;
    private $configuration;
    private $logger;
    private $session;
    private $scriptManager;
    private $translationHandler;
    
    /**
     * Constructor que inicializa los objetos necesarios para la aplicacion
     * @param array $config - array con la configuracion de la app
     */
    public function __construct(){
        $this->configuration = new Configuration();
        include_once ACTIVERECORD_CLASSPATH;
        $arCfg = array(
            'conectionString' => $this->configuration->getValue('dbConnectionString'),
            'dbCode' => $this->configuration->getValue('dbCode')
        );
        ActiveRecord\Config::initialize(function($cfg) use ($arCfg)  {
            $cfg->set_connections($arCfg['conectionString']);
            $cfg->set_default_connection($arCfg['dbCode']);
        });
        //TODO Test connection or die
        $this->logger = new Logger(
                $this->configuration->getValue("logPath"),
                $this->configuration->getValue("logMode"),
                $this->configuration->getValue("logMaxSize")
        );
        $this->router = new Router();
        $this->session = new Session();
        $this->scriptManager = new ScriptManager();
        $this->translationHandler = new TranslationHandler(
                $this->configuration->getValue('language'),
                $this->configuration->getValue('langFilesPrefix'),
                $this->configuration->getValue('langFilesPath')
        );
    }
    /**
     * Inicializacion de la aplicacion
     */
    public static function init(){
        //Create the instance
        if(self::$instance == null){
            self::$instance = new App();
        }
    }
    
    /**
     * Procesamiento de solicitudes mediante los controladores
     */
    public static function run(){
        try {
            $request = filter_input(INPUT_GET, 'p',FILTER_SANITIZE_STRING);
            
            //App::getSession()->checkValidSession();
            
            if(!isset($request)){
                $request = self::getConfigValue('defaultPage');
            }
            return self::processRequest($request);
        } catch (Exception $exc) {
            return self::handleError($exc);
        }
    }
    /**
     * Carga de paginas en funcion de los parametros.
     * 
     * @param type $name ruta de la pagina a cargar
     * @param type $data data a ser enviado a dicha pagina
     * @param type $return boolean para saber si se ha de mostrar directamente o devolverla(util para devoluciones ajax)
     */
    public static function loadPage($name,$data=null,$return = false){
        try {
            //Redirect to page
            if($return){
                //Devolver la vista solicitada
                return self::$instance->router->loadPage($name,$data,$return);
            }else{
                //Almacenar la vista solicitada
                $page = self::$instance->router->loadPage($name,$data,true);
                //Pasarla al layout principal
                return self::$instance->router->loadPage(self::getConfigValue('layoutPage'),array_merge($data,array('mainContent'=>$page)));
            }
        } catch (Exception $exc) {
            return self::handleError($exc);
        }
    }
    /**
     * Wrapper para obtener mediante el router una url a una pagina concreta
     * @param string $page pagina solicitada
     * @return string resultado
     */
    public static function getUrlToPage($page,$external = false){
        if($external){
            return self::$instance->router->createExternalUrl($page,$external);
        }
        return self::$instance->router->createUrlToPage($page);
    }
    /**
     * Wrapper para obtener un valor de configuracion mediante un key dado
     * @param string $key valor a obtener
     * @return valor obtenido
     */
    public static function getConfigValue($key){
        return self::$instance->configuration->getValue($key);
    }
    /**
     * Getter para obtener el objeto session de la app
     * @return Session
     */
    public static function getSession(){
        return self::$instance->session;
    }
    /**
     * Wrapper para procesar una request mediante el Router
     * @param String $request - pagina que se solicita
     * @param Array $data - datos de la solicitud
     */
    public static function processRequest($request,$data=null){
        return self::$instance->router->processRequest($request,$data);
    }
    /**
     * Getter para obtener la clase de log
     * @return Logger
     */
    public static function getLogger(){
        return self::$instance->logger;
    }
    /**
     * Resgistra un script en el script manager
     * @param String script path
     */
    public static function registerScript($script){
        self::$instance->scriptManager->registerScript($script);
    }
    /**
     * Resgistra un estilo en el script manager
     * @param String $style
     */
    public static function registerStyle($style){
        self::$instance->scriptManager->registerStylesheet($style);
    }
    /**
     * Devuelve todos los scripts registrados hasta el momento
     */
    public static function computeScripts(){
        echo self::$instance->scriptManager->computeRegisteredScripts();
    }
    /**
     * Devuelve todos los estilos registrados hasta el momento
     */
    public static function computeStyles(){
        echo self::$instance->scriptManager->computeRegisteredStylesheets();
    }
    /**
     * Devuelve el tipo de solicitud
     * @return boolean
     */
    public static function isAjaxRequest(){
        return filter_input(INPUT_SERVER,'HTTP_X_REQUESTED_WITH') !== null
        && strtolower(filter_input(INPUT_SERVER,'HTTP_X_REQUESTED_WITH')) === 'xmlhttprequest';
    }
    /**
     * Devuelve el valor traducido para una key dada
     * 
     * @param type $key
     * @return type
     */
    public static function translate($key){
        return self::$instance->translationHandler->translate($key);
    }
    
    private static function handleError($exc){
        
        $message = "<b>".$exc->getMessage()."</b>";
        if(self::getLogger()->getLogMode() == Logger::LOGLEVEL_DBG){
            $message .= "<br/>".$exc->getTraceAsString();
        }
        self::getLogger()->error($message);
        if(self::isAjaxRequest()){
            $response = array(
                'message'=>$message,
                'status'=>500,
                'success'=>false
            );
            echo json_encode($response);
            exit;
        }
        return self::processRequest(self::getConfigValue('errorPage'),array('error'=>$message));
    }
}
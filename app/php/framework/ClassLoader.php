<?php
class ClassLoader {

    private static $SAVE_FILE = 'classlist.temp.json';
    
    /* singleton */
    private static $instance;

    /* stores a className -> filePath map */
    private $classList;
    /* tells whether working from saved file */
    private $refreshed;

    private $exclusions = array();

    public static function getInstance() {
        if (!isset(self::$instance)) {
            self::$instance = new ClassLoader();
        }
        return self::$instance;
    }
    
    private function __construct() {
        $this->exclusions = array(EXCLUDES_DIR);
        $this->initClassList();
    }
    
    /**
     * Metodo registrado como autoloader que carga las clases en funcion
     * de un array leido.
     * 
     * 
     * @param type $className
     */
    public function loadClass($className) {
        
        if (!isset($this->classList) || !array_key_exists($className, $this->classList) && !$this->refreshed ) {
            $this->refreshClassList();
        }
        if(isset($this->classList[$className])){
            //throw new Exception('Class not found: '.$className);
            require_once($this->classList[$className]);
        }else{
            throw new Exception('Class not found: '.$className);
        }
        
    }
    /**
     * Inicializa el classlist leyendo los datos y guardandolos en una cache
     */
    private function initClassList() {
        if (file_exists(INCLUDES_DIR . self::$SAVE_FILE)) {
            $this->loadJsonFile();
            $this->refreshed = FALSE;
        } else {
            $this->refreshClassList();
        } 
    }
    
    private function loadJsonFile(){
        $file_content = file_get_contents(INCLUDES_DIR . self::$SAVE_FILE);
        
        $this->classList = json_decode($file_content, true);
    }
    
    /**
     * Recarga la cache de clases
     */
    private function refreshClassList() {
        $this->classList = $this->scanDirectory(INCLUDES_DIR);
        $this->refreshed = TRUE;

        $this->saveClassList();
    }

    /**
     * Guarda el archvio de clases
     */
    private function saveClassList() {
        $handle = fopen(INCLUDES_DIR . self::$SAVE_FILE, 'w');

        fwrite($handle,json_encode($this->classList));
        fclose($handle);
    }
    /**
     * Escanea el directorio especificado y almacena las clases y sus rutas en un array.
     * 
     * @param type $directory
     * @return string
     */
    private function scanDirectory ($directory) {
        // strip closing '/'
        if (substr($directory, -1) == '/') {
            $directory = substr($directory, 0, -1);
        }
        if (!file_exists($directory) || !is_dir($directory) || !is_readable($directory) || in_array($directory, $this->exclusions)) {
            return array();
        }
        $dirH = opendir($directory);
        $scanRes = array();

        while(($file = readdir($dirH)) !== FALSE) {
            // skip pointers
            if ( strcmp($file , '.') == 0 || strcmp($file , '..') == 0) {
                continue;
            }
            $path = $directory . '/' . $file;
            if (!is_readable($path)) {
                continue;
            }
            // recursion
            if (is_dir($path)) {
                $scanRes = array_merge($scanRes, $this->scanDirectory($path));

            } elseif (is_file($path)) {
                $className = explode('.', $file);
                if ( strcmp($className[1], 'php') == 0 ) {
                    $scanRes[$className[0]] = $path; 
                }
            }
        }
        
        return $scanRes;
    }

}

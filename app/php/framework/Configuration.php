<?php
 class Configuration{
    private $cfgValues = array();
    
    public function __construct(){
        $this->readConfigFile();
    }
    
    public function getValue($key){
        if(isset($this->cfgValues[$key])){
                return $this->cfgValues[$key];
        }else{
            return "";
        }
    }
    
    private function readConfigFile(){
        
        if (!file_exists(CONFIG_FILE)) {
          throw new Exception("No se puede cargar el fichero de configuracion");
        }
        $lang_file_content = file_get_contents(CONFIG_FILE);
        /* Load the language file as a JSON object 
           and transform it into an associative array */
        $this->cfgValues = json_decode($lang_file_content, true);
    }
 }
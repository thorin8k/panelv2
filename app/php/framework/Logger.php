<?php
/**
 * Logger class.
 * 
 * @author Thorin
 * @version 0.1
 */
class Logger {
        const NOTICE='NOTICE';
        const WARNING='WARNING';
        const ERROR='ERROR';
        const FATAL='FATAL';
        const ACCESS='ACCESS';
        const LOGLEVEL_DBG='DEBUG';
        const LOGLEVEL_ERROR='ERROR';
        const LOGLEVEL_INFO='INFO';
        
        private $file = '';
        private $logMode = Logger::ERROR;
        private $maxFileSize= 1310720;
        
        public function __construct($file='',$logMode = Logger::ERROR, $maxFileSize = 1310720){
            $this->setFile($file);
            $this->setLogMode($logMode);
            $this->setMaxFileSize($maxFileSize);
        }
        /**
         * Guarda un mensaje con un tipo determinado
         */
        public function log($message,$messageType=Logger::WARNING) {
            $msg = "[".$this->getTime()."] $messageType - $message \n";
            
            if($messageType == Logger::ACCESS){
                $this->writeToLog($msg,'access');
                return;
            }
            
            switch ($this->logMode) {
                case Logger::LOGLEVEL_ERROR:
                    if($messageType == Logger::ERROR
                        || $messageType == Logger::FATAL){
                        $this->writeToLog($msg);
                    }
                    break;
                case Logger::LOGLEVEL_INFO:
                    if($messageType == Logger::ERROR
                        || $messageType == Logger::WARNING){
                        $this->writeToLog($msg);
                    }
                    break;
                case Logger::LOGLEVEL_DBG: 
                    if($messageType == Logger::NOTICE 
                        || $messageType == Logger::WARNING
                        || $messageType == Logger::ERROR
                        || $messageType == Logger::FATAL){
                        $this->writeToLog($msg);
                    }
                    break;
            }
        }
        /**
         * Escribe en el archivo de log
         * @param type $msg
         */
        public function writeToLog($msg, $type = 'error'){
            //Check filesize to archive
            $this->fileMaintenance();
            if(!empty($this->file)){
                file_put_contents($this->file[$type], $msg, FILE_APPEND | LOCK_EX);
            }
        }
        /**
         * Comprueba que el archivo no excede el tamaño configurado.
         * Si lo excede se creará uno nuevo y se archivará el existente.
         */
        public function fileMaintenance(){
            foreach($this->file as $file){
                if(file_exists($file)){
                    $actualFileSize = filesize($file);
                    if($actualFileSize > $this->maxFileSize){
                        rename($file,  str_replace('.', '-'.date('Ymd-hi').'.', $file));
                    }
                }
            }
        }
        /**
         * Setter para el archivo
         * @param type $file
         */
        public function setFile($file){
            $this->file = $file;
        }
        /**
         * Tipo de funcionamiento del log, tipos de trazas a tener en cuenta.
         * @param type $logMode
         */
        public function setLogMode($logMode) {
            $this->logMode = $logMode;
        }
        /**
         * Devuelve el tipo de funcionamiento.
         * @return type
         */
        public function getLogMode() {
            return $this->logMode;
        }
        /**
         * Tamaño máximo de cada archivo de log
         * @param type $maxFileSize
         */
        public function setMaxFileSize($maxFileSize) {
            $this->maxFileSize = $maxFileSize;
        }

        /**
         * Returns the current timestamp in dd.mm.YYYY - HH:MM:SS format
         * @return string with the current date
         */
        private function getTime() {
            return date("d/m/Y  H:i:s");
        }
               
        /**
         * Convenience function to wrap logger->log($message,$messagetype);
         * @param string $message
         */
        public function notice($message) {
            $this->log($message,Logger::NOTICE);
        }
        
        /**
         * Convenience function to wrap logger->log($message,$messagetype);
         * @param string $message
         */
        public function warn($message) {
            $this->log($message,Logger::WARNING);
        }
        
        /**
         * Convenience function to wrap logger->log($message,$messagetype);
         * @param string $message
         */
        public function error($message) {
            $this->log($message,Logger::ERROR);
        }
        
        /**
         * Convenience function to wrap logger->log($message,$messagetype);
         * @param string $message
         */
        public function fatal($message) {
            $this->log($message,Logger::FATAL);
        }
        /**
         * Funcion especial de log que almacena un acceso en base de datos
         * 
         * @param type $message
         */
        public function access($message){
            $this->log($message,Logger::ACCESS);
        }
}
<?php
class Router {
    /**
     * Separa la request en controlador/accion para crear una instancia del controlador
     * solicitado y llamar al metodo definido en la accion pasandole $params. 
     * 
     * @param string $request
     * @param array $params 
     * @return type
     * @throws Exception
     */
    public function processRequest($request,$params=null){
        $urlParams = explode('/', $request);
        $controller = ucfirst(array_shift($urlParams)).'Controller';
        $action = strtolower(array_shift($urlParams)).'Action';
        $ctrlName = $controller;
        if(!class_exists($ctrlName)) {
            throw new Exception("Error loading page. The requested controller does not exists");
        }
        $ctrl = new $ctrlName($request);
        $data = null;
        if(is_callable(array($ctrl, $action))){
            $data = $ctrl->$action($params);
        }
        return $data;
        
    }
    /**
     * Incluye la vista indicada en $pageName pasandole los datos
     * 
     * @param type $pageName
     * @param type $_data_
     * @param type $_return_
     * @return type
     * @throws Exception
     */
    public function loadPage($pageName,$_data_=null,$_return_=false){
        //Data is extracted and passed to the view
        if(is_array($_data_)){
            extract($_data_,EXTR_PREFIX_SAME,'data');
        }else{
            $data=$_data_;
        }
        //Search filename to import
        $array = $this->searchPageInFileSystem(trim($pageName));
        //Check and import
        if(count($array) != 0){
            if($_return_) {
                ob_start();
                ob_implicit_flush(false);
                require($array[0]);
                return ob_get_clean();
            }else{
                require($array[0]);
            }
        }else{
            throw new Exception("Error loading page. Page not found");
        }
    }
    /**
     * Obtiene la ruta a una vista
     * 
     * @param type $pageName
     * @return type
     */
    private function searchPageInFileSystem($pageName){
        $file = glob(VIEW_PATH.$pageName.".php");
        return $file;
    }
    /**
     * Devuelve el enlace para la vista 
     * 
     * @param String $page
     * @return type
     */
    public function createUrlToPage($page){
        return "index.php?p=$page";
    }
    /**
     * Devuelve en enlace para la vista solicitada para llamadas externas.
     * 
     * @param type $page
     * @return type
     */
    public function createExternalUrl($page){
        return "index.php?e=$page";
    }
}


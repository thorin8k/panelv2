<?php
class ScriptManager{
    
    private $scripts = array();
    private $printedScripts = array();
    private $styles = array();
    private $printedStyles = array();
    /**
     * Inserta un script para ser cargado posteriormente.
     * 
     * @param String $script
     * @param int $sort
     */
    public function registerScript($script,$sort=0){
        if(!is_array($script)){
            array_push($this->scripts, array('value'=>$script,'sort'=>$sort));
        }
    }
    
    /**
     * Inserta un estilo para ser cargado posteriormente.
     * 
     * @param String $script
     * @param int $sort
     */
    public function registerStylesheet($stylesheet,$sort=0){
        if(!is_array($stylesheet)){
            array_push($this->styles, array('value'=>$stylesheet,'sort'=>$sort));
        }
    }
    /**
     * Devuelve todos los scripts con sus respectivos tag para cargarlos en html y
     * vacia la cache de scripts por cargar.
     * 
     * @return string
     */
    public function computeRegisteredScripts(){
        //TODO sort
        $result = "";
        foreach ($this->scripts as $script) {
            $result.="<script type='text/javascript' src='".$script['value']."'></script>\n";
            $this->printedScripts[] = $script;
        }
        //Clear temp values
        $this->scripts = array();
        
        return $result;
    }
    /**
     * Devuelve todos los estilos con sus respectivos tag para cargarlos en html y
     * vacia la cache de estilos por cargar.
     * 
     * @return string
     */
    public function computeRegisteredStylesheets(){
        //TODO sort
        $result = "";
        foreach ($this->styles as $style) {
            $result.="<link rel='stylesheet' type='text/css' media='all' href='".$style['value']."' />\n";
            $this->printedStyles[] = $style;
        }
        //Clear temp values
        $this->styles = array();
        
        return $result;
    }
}


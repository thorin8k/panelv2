<?php
class Session{
    
    public function __construct(){
        session_start();
    }
    /**
     * Inicia sesión con un usuario y un password dados. 
     * 
     * Devuelve true si el inicio ha sido correcto o false si ha ocurrido algun problema
     * @param String $user
     * @param String $password
     * @return boolean
     */
    public function login($user,$password){

        //TODO rebuild this method
        $password = ($password);
        if($user == 'admin' && $password == 'admin'){
            $_SESSION['user'] = $user;
            $_SESSION['appUID'] = md5(App::getConfigValue('name'));
            $_SESSION['time'] = FrameworkUtils::RetrieveTime();
            return true;
        }else{
            if(App::getConfigValue('useLdap')){
                $this->configureLdap();
                $test = $this->ldapLogin($user, $password);
                if(!$test){
                    App::getLogger()->log($this->getLastError(),  Logger::ERROR);
                    return false;
                }
                $_SESSION['user'] = $user;
                $_SESSION['appUID'] = md5(App::getConfigValue('name'));
                $_SESSION['time'] = FrameworkUtils::RetrieveTime();
                return true;
            }
        }
        return false;
    }
    /**
     * Destruye la sesion del usuario actual
     */
    public function logout(){
        $_SESSION['user'] = '';
        session_destroy();
    }
    /**
     * Comprueba si hay un usuario logueado
     * @return boolean
     */
    public function isGuestSession(){
        if(isset($_SESSION['appUID']) && $_SESSION['appUID'] == md5(App::getConfigValue('name'))){
            if(isset($_SESSION['user']) && $_SESSION['user'] != ''){
                return false;
            }else{
                return true;
            }
        }else{
            return true;
        }
    }
    /**
     * Obtiene el nombre del usuario actual
     * @return String
     */
    public function getUserName(){
        return isset($_SESSION['user']) ? $_SESSION['user'] : null;
    }
     
    /**
     * 
     * @return boolean
     */
    public function checkValidSession(){
        $cfgValidPeriod = App::getConfigValue('sessionExpiration');
        $currTime = FrameworkUtils::RetrieveTime();
        if(isset($_SESSION['time'])){
            $diff = $currTime - $_SESSION['time'];
            if($diff > $cfgValidPeriod){
                $this->logout();
                if(App::isAjaxRequest()){
                    $response = array(
                        'message'=>'La sesión ha expirado.',
                        'status'=>401,
                        'success'=>false
                    );

                    echo json_encode($response);
                    exit;
                }
            }
        }
    }
    
    public function getUserMenu(){
        $arrayRes = array();
        if(!$this->isGuestSession()){
            $userDao = new UserDao();
            $user = $userDao->getUser($_SESSION['user']);
            $menuEntrys = $user!== null ?  ($user->profile != null ? $user->profile->menu : array()) : array();
            if(!empty($menuEntrys)){
                foreach ($menuEntrys as $value) {
                    $arrayRes[]=array('text'=>$value->text, 'page'=>$value->page);
                }
            }
        }
        return $arrayRes;
    }
    
    
    public function getActualUser(){
        $userDao = new UserDao();
        return $userDao->getUser($_SESSION['user']);
    }
    
    public function getUserObject(){
        if(isset($_SESSION['user'])){
            $userDao = new UserDao();
            $userObj = $userDao->getUser($_SESSION['user']);
            return $userObj;
        }
        return null;
    }
    
    public function isAdmin(){
        return isset($_SESSION['appUID']) 
        && $_SESSION['appUID'] == md5(App::getConfigValue('name')) 
        && isset($_SESSION['user']) 
        && $_SESSION['user'] == 'admin';
    }
    
    //ADLdap 
    public $adldap; 
    
    private $ldapConn;


    public function configureLdap(){
        $this->adldap = new adLDAP(array(
            'base_dn'=>App::getConfigValue("ldapBaseDn"),
            'domain_controllers'=>App::getConfigValue('ldapHost'),
            'account_suffix'=>'',
            'admin_username'=>App::getConfigValue('ldapAdminCN').','.App::getConfigValue("ldapBaseDn"),
            'admin_password'=>App::getConfigValue('ldapAdminPassword'),
            'use_ssl'=>false,
            'use_tsl'=>false,
        ));
    }
    public function ldapLogin($username,$password){
        return $this->adldap->authenticate("cn=".$username.",".App::getConfigValue('ldapMainGroup').','.App::getConfigValue("ldapBaseDn"), ($password),true);
    }
    public function createUser($username,$password){
        $attributes = array(
            'objectClass'=>'inetOrgPerson',
            'cn'=>$username,
            'sn'=>$username,
            'userPassword'=>"{md5}".base64_encode(pack("H*",md5($password))),
            'displayName'=>$username,
            'givenName'=>$username,
            'audio'=>'20GB'
        );
        
        
        $result = ldap_add(
            $this->adldap->getLdapConnection(),
            "CN=" . $username.",".App::getConfigValue('ldapMainGroup').','.App::getConfigValue("ldapBaseDn"),
            $attributes
        );
        return $result;
    }
    public function getLastError(){
        return $this->adldap->getLastError();
    }
    
    public function changePassword($username,$password){
        $attributes = array(
            'userPassword'=>"{md5}".base64_encode(pack("H*",md5($password)))
        );
        ldap_mod_replace (
            $this->adldap->getLdapConnection(),
            "CN=" . $username.",".App::getConfigValue('ldapMainGroup').','.App::getConfigValue("ldapBaseDn"),
            $attributes
        );
        
    }
}

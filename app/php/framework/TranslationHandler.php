<?php
class TranslationHandler{
    
    private $filePath = '';
    private $filePrefix = '';
    
    private $translations = array();
    private $lang = '';
    private $defaultLang = 'es';
    
    public function __construct($lang,$filePrefix,$filePath) {
        $this->lang = $lang;
        $this->filePath = $filePath;
        $this->filePrefix = $filePrefix;
        if($this->translations == null){
            $this->readTranslationsFile();
        }
    }
    /**
     * Devuelve el texto correspondiente a la key dada
     * @param String $key
     * @return String
     */
    public function translate($key){
        if($this->translations == null){
            $this->readTranslationsFile();
        }
        return isset($this->translations[$key]) ? $this->translations[$key] : $key ;
    }
    /**
     * Recarga el archivo de traducciones en función del idioma actual
     */
    private function readTranslationsFile(){
        $lang_file = $this->filePath.'/'.$this->filePrefix.$this->lang.'.json';
        /* If no instance of $translations has occured load the language file */
        if (!file_exists($lang_file)) {
          $lang_file = $this->filePath.'/'.$this->filePrefix.$this->defaultLang.'.json';
        }
        $lang_file_content = file_get_contents($lang_file);
        /* Load the language file as a JSON object 
           and transform it into an associative array */
        $this->translations = json_decode($lang_file_content, true);
    }
    /**
     * Establece el idioma actual. Para que la configuracion sea válida debe 
     * existir un archivo correcto en la carpeta configurada. pEj: i18_es.json
     * si se establece 'es' como idioma.
     * 
     * Tambien recarga las traducciones para poder cambiar el idioma en "caliente"
     * 
     * @param String $lang
     */
    public function setLang($lang){
        $this->lang = $lang;
        $this->readTranslationsFile();
    }
    
    public function setFilePath($JSON_PATH) {
        $this->filePath = $JSON_PATH;
    }

    public function setFilePrefix($FILE_PREFIX) {
        $this->filePrefix = $FILE_PREFIX;
    }


    
}


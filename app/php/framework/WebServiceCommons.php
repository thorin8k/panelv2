<?php
trait WebServiceCommons {
    
    private $client;
    
    /**
     * Crea un SoapClient para la location definida.
     * @param type $location
     */
    public function createClient($location){
        $this->client  = new SoapClient(null, array(
            'location'=> $location,
            'uri'     => "http://tempuri.org"
        ));
    }
    /**
     * Obtener datos del webservice actual llamando al metodo especificado y con 
     * los parametros pasados
     * 
     * @param string $methodName
     * @param array $params
     * @param boolean $withoutLogout - parametro para evitar nesting de llamadas en la llamada para hacer logout
     * @return Object
     */
    public function generateCall($methodName, $params){
        $requestParameters= array();
        foreach ($params as $key => $value) {
            if ($value == null) {  continue;  }
            array_push($requestParameters, new SoapParam($value, $key));
        }
        $result = $this->client->__soapCall($methodName,
            $requestParameters,
            array('soapaction' => 'http://tempuri.org/'.$methodName)
        );
        App::getLogger()->log('Llamando al método '.$methodName, Logger::NOTICE);
        return $result;
    }
}


<?php
class CacheManager{
    
    private static $path = 'app/tmp/';
    public $caches = array();
    
    
    function __construct($path) {
        $this->updateConfigPath($path);
        $this->loadCacheDir();
        $this->reloadExpiredCaches();
        $this->clearExpiredCaches();
    }

    /**
     * Carca las caches existentes en el directorio
     */
    private function loadCacheDir(){
        
        
        $files = glob(self::$path.'*'); // get all file names
        foreach($files as $file){ // iterate files
            if(is_file($file)){
                $cacheName = substr($file, strrpos($file, '/')+1);
                $this->caches[$cacheName] = new CacheStore($file);
            }
        }
    }
    
    /**
     * 
     */
    public function reload(){
        $this->loadCacheDir();
    }
    
    /**
     * 
     * @param type $identifier
     * @return CacheStore
     */
    public function addCache($identifier){
        if(array_key_exists($identifier, $this->caches)){
            return $this->getCache($identifier);
        }else{
            return $this->caches[$identifier] = new CacheStore(self::$path.$identifier);
        }
    }
    
    /**
     * Devuelve un objeto cache.
     * 
     * @param string $identifier
     * @return CacheStore
     */
    public function getCache($identifier){
        
        return $this->caches[$identifier];
    }
    
    /**
     * Limpia las caches que han expirado
     */
    public function clearExpiredCaches(){
        foreach($this->caches as $key=>$cache){
            if($cache->hasExpired()){
                $cache->deleteCache();
                unset($this->caches[$key]);
            }
        }
    }
    
    /**
     * Actualiza desde la configuracion el valor del directorio temporal.
     * 
     */
    private function updateConfigPath($cfgValue){
        if(!empty($cfgValue)){
            self::$path = $cfgValue;
        }
    }
    
    
    
    /**
     * Limpia el contenido del directorio temporal. Util para tareas administrativas
     * 
     */
    public static function clearCacheDir(){
        $this->updateConfigPath();
        
        $files = glob(self::$path.'*'); // get all file names
        foreach($files as $file){ // iterate files
            if(is_file($file)){
                unlink($file);
            }
        }
    }
    
    private function reloadExpiredCaches(){
        //TODO recargar las caches que han expirado.
        //Quizas era bueno utilizar un thread o algo...
    }
    
}


<?php
class CacheStore{
    
    private $cacheExpirationTime = 100;//Tiempo en "" de validez de los archivos
    private $handler;
    
    function __construct($file) {
        $this->handler = new FileHandler($file);
    }
    /**
     * Almacena la variable data serializada en la cache definida por el constructor.
     * Durante la escritura del archivo este se bloquea.
     * 
     * Toda la informacion de la cache es sobreescrita cada vez que se llama a 
     * este metodo
     * 
     * @param mixed $data
     * @throws FileNotDefinedException
     * @throws StoreLockedException
     */
    public function storeOnCache($data){
        $this->checkValidity();
        $this->checkExpiration();
        //No es necesario comprobar la validez ya qe se sobreescribe siempre.
        
        $this->handler->writeLock();
        $this->handler->overwrite(serialize($data));
        $this->handler->unlock();
        
    }
    /**
     * Almacena la variable data serializada en la cache definida por el constructor.
     * Durante la escritura del archivo este se bloquea.
     * 
     * El contenido previo de la cache es unido con el contenido de la variable data y
     * el resultado se almacena.
     * 
     * @param mixed $data
     * @throws FileNotDefinedException
     * @throws StoreLockedException
     */
    public function appendToCache($data){
        $this->checkValidity();
        $this->checkExpiration();
        
        $this->handler->writeLock();
        $prevData = unserialize($this->handler->read());
        $newData = $data;
        if(!empty($prevData)){
            $newData = array_merge($prevData, $data);
        }
        $this->handler->overwrite(serialize($newData));
        $this->handler->unlock();
        
    }
    /**
     * Lee la información almacenada en la cache, la deserializa y la devuelve.
     * 
     * Durante la lectura el archivo es bloqueado de forma compartida(readlock)
     * 
     * @return mixed
     * @throws FileNotDefinedException
     * @throws FileNotFoundException
     * @throws StoreLockedException
     */
    public function loadFromCache(){
        $this->checkValidity();
        $this->checkExpiration();
        if(!$this->handler->exists()){
            throw new FileNotFoundException();
        }
        
        $this->handler->readLock();
        $result = unserialize($this->handler->read());
        $this->handler->unlock();
        
        return $result;
    }
    
    /**
     * Limpia el archivo de cache actual.
     * 
     * @throws FileNotFoundException
     */
    public function clearCache(){
        if(!$this->handler->exists()){
            throw new FileNotFoundException();
        }
        $this->handler->writeLock();
        $this->handler->clear();
        $this->handler->unlock();
    }
    
    /**
     * Borra el archivo de cache
     */
    public function deleteCache(){
        $this->handler->delete();
    }
    /**
     * Limpia la cache en caso de pasar el tiempo de expiración
     */
    private function checkExpiration(){
        if($this->handler->exists() && $this->hasExpired()){
            //FIXME que hago, lo borro o no?
            $this->clearCache();
        }
    }
    /**
     * Comprueba el tiempo de expiración.
     * @return boolean
     */
    public function hasExpired(){
        $actualTime = time();
        $fileTime = $this->handler->getFileModificationTime();
        $total_seconds = floor($actualTime - $fileTime);
        
        if($total_seconds > $this->cacheExpirationTime){
            return true;
        }
        return false;
    }
    /**
     * Comprueba que el archivo se ha definido, existe y no esta bloqueado. 
     * 
     * @throws FileNotDefinedException
     * @throws FileNotFoundException
     * @throws StoreLockedException
     */
    private function checkValidity(){
        if($this->handler->getFile() === null){
            throw new FileNotDefinedException();
        }
        if($this->handler->isLocked()){
            throw new StoreLockedException();
        }
    }
    
    /**
     * Establece el archivo de cache.
     * 
     * @param string $file
     */
    public function setFile($file){
        $this->handler->setFile($file);
    }
    
    /**
     * Obtiene el archivo de cache.
     * @return string
     */
    public function getFile(){
        return $this->handler->getFile();
    }
    
    /**
     * Obtiene el tiempo de expiracion
     * @return string
     */
    public function getCacheExpirationTime() {
        return $this->cacheExpirationTime;
    }
    /**
     * Obtiene la fecha de ultima modificacion del archivo con formato legible
     * @return type
     */
    public function getLastModifDate(){
        if(!$this->handler->exists()){
            throw new FileNotFoundException();
        }
        return date('d/m/Y H:i:s', $this->handler->getFileModificationTime());
    }
    
    /**
     * Establece el tiempo de expiracion 
     * @param type $cacheExpirationTime
     */
    public function setCacheExpirationTime($cacheExpirationTime) {
        $this->cacheExpirationTime = $cacheExpirationTime;
    }
        
    /**
     * Devuelve true si el archivo se encuentra bloqueado y false si no.
     * @return boolean
     */
    public function isLocked(){
        return $this->handler->isLocked();
    }

    
}


class StoreLockedException extends Exception{
    protected $message = 'La store se encuentra bloqueada actualmente.';
    protected $code = 666;
}

class FileNotDefinedException extends Exception{
    protected $message = 'Es necesario definir un nombre de archivo para la cache.';
    protected $code = 667;
}
class FileNotFoundException extends Exception{
    protected $message = 'El archivo no existe o no se encuentra en la ruta especificada.';
    protected $code = 668;
}

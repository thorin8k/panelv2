<?php
class FileHandler {

    private $file;
    
    public function __construct($file) {
        $this->file = $file;
        //en caso de que no exista abre y cierra el archivo para crearlo
        if(!$this->exists()){
            $handle = fopen($this->file, 'w');
            fclose($handle);
        }
    }
    /**
     * Establece un bloqueo ligero para lecturas.
     * 
     * @return boolean
     */
    public function readLock(){
        $handle = fopen($this->file, 'r');
        $result = flock($handle,LOCK_SH);
        fflush($handle);
        fclose($handle);
        clearstatcache();
        return $result;
    }
    
    /**
     * Establece un bloqueo completo para escrituras.
     * @return boolean
     */
    public function writeLock(){
        $handle = fopen($this->file, 'r+');
        $result = flock($handle,LOCK_EX);
        fflush($handle);
        fclose($handle);
        clearstatcache();
        return $result;
    }
    
    /**
     * Comprueba el estado de bloqueo del archivo.
     * 
     * @return boolean
     */
    public function isLocked(){
        if ($this->writeLock()) {//lock was successful
            $this->unlock();
            return false;
        }else{
            return true;
        }
    }
    /**
     * Desbloquea el archivo
     */
    public function unlock(){
        $handle = fopen($this->file, 'r+');
        flock($handle,LOCK_UN);
        fflush($handle);
        fclose($handle);
        clearstatcache();
    }
    /**
     * Sobreescribe el contenido del archivo con los datos pasados en $data
     * 
     * @param string $data
     */
    public function overwrite($data){
        $handle = fopen($this->file, 'w');
        fwrite($handle,$data);
        fflush($handle);
        fclose($handle);
        clearstatcache();
    }
    /**
     * Limpia el contenido del archivo 
     */
    public function clear(){
        $handle = fopen($this->file, 'w');
        fflush($handle);
        fclose($handle);
        clearstatcache();
    }
    
    /**
     * Borra el archivo del disco
     */
    public function delete(){
        unlink($this->file);
        clearstatcache();
    }
    
    /**
     * Devuelve el contenido del archivo
     * 
     * @return mixed
     */
    public function read(){
        return file_get_contents($this->file);
    }
    
    /**
     * Comprueba si el archivo existe en el disco
     * @return boolean
     */
    public function exists(){
        return file_exists($this->file);
    }
    
    /**
     * Añade el contenido de la variable $data al ya existente en el archivo.
     * 
     * @param string $data
     */
    public function append($data){
        $handle = fopen($this->file, 'a');
        fwrite($handle,$data);
        fflush($handle);
        fclose($handle);
        clearstatcache();
    }
    
    /**
     * Establece la ruta del archivo a utilizar
     * 
     * @param string $file
     */
    public function setFile($file) {
        $this->file = $file;
    }
    
    /**
     * Obtiene la ruta del archivo usada actualmente por la clase
     * @return string
     */
    public function getFile() {
        return $this->file;
    }
    
    /**
     * Obtiene la fecha de ultima modificacion del archivo.
     * @return time
     */
    public function getFileModificationTime(){
        return filemtime($this->file);
    }
    

}


<?php
class FrameworkUtils{
    
    public static function HumanFileSize($bytes, $decimals = 2) {
        $sz = 'BKMGTP';
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
    }
    
    public static function RetrieveTime(){
        $mtime = microtime(); 
        $mtime = explode(' ',$mtime); 
        $mtime = $mtime[1] + $mtime[0];

        return $mtime;
    }
    
    public static function CalculateLoadTime ($starttime, $endtime){
        $totaltime = ($endtime - $starttime); 
        return 'This page was created in '.$totaltime.' seconds.'; 
    }
    
    public static function MemoryUsage() { 
        $mem_usage = memory_get_usage(true); 
        $res="";
        $res.= self::HumanFileSize($mem_usage);
            
        //$res.= "<br/>"; 
        return $res;
    }
    
    /**
     *
     *  Transforms the date from db to GUI or from GUI to db
     *
     *  dd/mm/yyyy
     * @param <type> $fecha
     * @return <type> parsed Date <string>
     */
    public static function parseDate($fecha) {
       
        if($fecha != ""){
            if (strlen($fecha)==10 && strpos($fecha, "-")){
                if (($fecha == "") or ($fecha == "0000-00-00") or ($fecha == "NULL")){
                    return "";
                }else{
                    return date("d/m/Y",strtotime($fecha));
                }
            }else{
                if ($fecha<>""){
                $trozos=explode("/",$fecha,3);
                return "".$trozos[2]."-".$trozos[1]."-".$trozos[0]."";
                }else{
                    return "";
                }
            }
        }
    }
}

<?php
class HtmlGenerator{
    public static function createTable($tableId,$data,$columns,$options){
        $actionColumns = array(
            'edit'=> array('icon'=>'icon-pencil','callback'=>'defaultEditCallback($(this))','title'=>'Modificar'),
            'delete'=>array('icon'=>'icon-remove','callback'=>'defaultDeleteCallback($(this))','title'=>'Eliminar')
        );
        $resultString = "<script>
            $(document).ready(function() {
                var oTable = $('#".$tableId."').dataTable( {
                    'sPaginationType': 'bootstrap',
                     'oLanguage': {
                       'sLengthMenu': 'Mostrar _MENU_ registros',
                       'sInfo': 'Obtenidos _TOTAL_ registros (_START_ de _END_)',
                       'sInfoEmpty': 'No hay registros para mostrar',
                       'sInfoFiltered': ' - de _MAX_ registros',
                       'sSearch': 'Filtrar registros:',
                       'sEmptyTable': 'No se han encontrado datos',
                       'sZeroRecords': 'No se han encontrado datos',
                       'oPaginate': {
                         'sFirst': ' ',
                         'sLast': ' ',
                         'sNext': ' ',
                         'sPrevious': ' '
                       }
                     }
                } );
                $('#".$tableId." .filter td').each( function ( i ) {
                    this.innerHTML = fnCreateSelect( oTable.fnGetColumnData(i) );
                    $('select', this).change( function () {
                        oTable.fnFilter( $(this).val(), i );
                    } );
                } );
            } );

         </script>";

        $resultString .= "<table id='".$tableId."' class='table table-bordered table-hover'>";
        //heads
        $resultString .= "<thead><tr>";
        foreach($columns as $col){
            $resultString .="<th>".$col['header']."</th>";
        }
        if(isset($options['columns'])){
            $resultString .= "<th></th>";
        }
        $resultString .= "</tr></thead>";
        //end heads

        //body
        $resultString .= "<tbody>";
        foreach($data as $id=>$row){
            $resultString .= "<tr>";
            foreach($columns as $col){
                $attribute = $col['field'];
                $resultString .= "<td>";
                if(strpos($attribute,'relation:')!== FALSE){
                    $attribute = str_replace('relation:', '', $attribute);
                    $firstAttrib = substr($attribute, 0, strpos($attribute, '-'));
                    $secAttrib = substr($attribute, strpos($attribute, '-')+1);

                    $resultString .= $row->$firstAttrib->$secAttrib;
                }else if(strpos($attribute,'parse:')!== FALSE){
                    $attribute = str_replace('parse:', '', $attribute);
                    $function = $col['parseFunction'];

                    $resultString .= call_user_func($function,$row->$attribute);
                }else{
                    if(isset($row->$attribute)){
                        $resultString .= $row->$attribute;
                    }
                }
                $resultString .= "</td>";
            }
            if(isset($options['columns'])){
                $resultString .= "<td>";
                foreach($options['columns'] as $key=>$action){
                    if(is_array($action)){
                        $resultString .= self::createGridActionButton($key, $id, $tableId, $action['icon'], $action['callback'], $action['title'],$row->id);
                    }else{
                        $resultString .= self::createGridActionButton($action, $id, $tableId, $actionColumns[$action]['icon'], $actionColumns[$action]['callback'], $actionColumns[$action]['title'],$row->id);
                    }
                }
                $resultString .= "</td>";
            }
            $resultString .= "</tr>";
        }
        $resultString .= "</tbody>";
        //endbody
        $resultString .= "<tr class='filter'>";
        foreach($columns as $elm){
            $resultString .= "<td></td>";
        }
        if(isset($options['columns'])){
            $resultString .= "<th></th>";
        }
        $resultString .= "</tr>";
        $resultString .="</table>";
        return $resultString;
    }

    private static function createGridActionButton($type,$value,$tableId,$icon,$callbackFunction,$title, $itemid){
        $id = $type."_".$tableId;
        return "<a id='$id' value='$value' itemid='$itemid' title='$title' onclick='$callbackFunction'><i class='$icon'></i></a>";
    }

    public static function createButton($id,$class,$text, $jsFunction = ""){
        return "<button class='$class' id='$id' onclick='$jsFunction'>$text</button>";
    }

    public static function createLink($url,$text, $jsFunction = ""){
        return "<a href='$url' onclick='$jsFunction'>$text</a>";
    }

    public static function createForm($id,$class, $model, $attributes){
        $resForm = "<form id='$id' class='$class'><div class='row-fluid'>";

        foreach($attributes as $field){
            if(empty($field)) continue;
            if(!isset($field['title'])) $field['title']='';
            if(!isset($field['type'])) $field['type']='text';
            if(!isset($field['required'])) $field['required']=false;
            if(!isset($field['attr'])) $field['attr']='';

            $class = isset($field['class']) ? $field['class'] : "";

            switch($field['type']){
                case 'text':
                    $resForm .= '<div class="form-control '.$class.'">';
                    $resForm .= '<label for="'.$field['attr']."_field".'">'.$field['title'].'</label>';
                    $resForm .= self::createTextInput(
                        $field['attr']."_field",
                        $model->$field['attr'],
                        get_class($model).'['.$field['attr'].']',
                        $field['required']
                    );
                     $resForm .= '</div>';
                    break;
                case 'password':
                    $resForm .= '<div class="form-control '.$class.'">';
                    $resForm .= '<label for="'.$field['attr']."_field".'">'.$field['title'].'</label>';
                    $resForm .= self::createPwdInput(
                        $field['attr']."_field",
                        $model->$field['attr'],
                        get_class($model).'['.$field['attr'].']',
                        $field['required']
                    );
                     $resForm .= '</div>';
                    break;
                case 'dropdown':
                    $resForm .= '<div class="form-control '.$class.'">';
                    $resForm .= '<label for="'.$field['attr']."_field".'">'.$field['title'].'</label>';
                    $resForm .= self::createDropdown(
                        $field['attr'].'_field',
                        $field['data'],
                        get_class($model).'['.$field['attr'].']',
                        $model->$field['attr'],
                        $field['required']
                    );
                     $resForm .= '</div>';
                    break;
                case 'datepicker':
                    $resForm .= '<div class="form-control '.$class.'">';
                    $resForm .= '<label for="'.$field['attr']."_field".'">'.$field['title'].'</label>';
                    $resForm .= self::createDatepicker(
                        $field['attr']."_field",
                        $model->$field['attr'],
                        get_class($model).'['.$field['attr'].']',
                        $field['required']
                    );
                     $resForm .= '</div>';
                    break;
                case 'textarea':
                    $resForm .= '<div class="form-control '.$class.'">';
                    $resForm .= '<label for="'.$field['attr']."_field".'">'.$field['title'].'</label>';
                    $resForm .= self::createTextArea(
                        $field['attr']."_field",
                        $model->$field['attr'],
                        get_class($model).'['.$field['attr'].']',
                        $field['required']
                    );
                     $resForm .= '</div>';
                    break;
                case 'spacer':
                    $style = isset($field['style']) ? $field['style'] : "width:175px";
                    $resForm .= '<div class="form-spacer" style="'.$style.'"><p>'.$field['title'].'&nbsp;</p></div>';
                    break;
                case 'separator':
                    $style = isset($field['style']) ? $field['style'] : "";
                    $resForm .= '<hr style="'.$style.'"/>';
                    break;
                case 'line-break':
                    $style = isset($field['style']) ? $field['style'] : "";
                    $resForm .= '<div class="row" style="'.$style.'"/>';
                    break;
                case 'html':
                    $resForm.= $field['data'];
                    break;
            }
        }
        $resForm .= "</div></form>";

        return $resForm;
    }

    public static function createTextArea($id, $value, $name, $required){
        $reqText = $required ? "req='true'" : "";
        return "<textarea $reqText type='text' name='$name' id='$id' >$value</textarea>";
    }
    public static function createTextInput($id, $value, $name, $required){
        $reqText = $required ? "req='true'" : "";
        return "<input $reqText type='text' name='$name' value='$value' id='$id' />";
    }
    public static function createPwdInput($id, $value, $name, $required){
        $reqText = $required ? "req='true'" : "";
        return "<input $reqText type='password' name='$name' value='$value' id='$id' />";
    }

    public static function createDropdown($id, $data, $name,  $selected, $required){
        $reqText = $required ? "req='true'" : "";
        $resSel = "<select class='combobox' $reqText name='$name' id='$id'>";
        foreach($data as $value=>$option){
            $isSelected = '';
            if($value == $selected){
                $isSelected='selected';
            }
            $resSel .= "<option $isSelected value='$value'>$option</option>";
        }
        $resSel .= "</select>";
        return $resSel;
    }

    public static function createDatepicker($id, $value, $name, $required){
        $reqText = $required ? "req='true'" : "";
        return "<input type='text' $reqText class='datepicker' name='$name' value='$value' id='$id' />";
    }
}


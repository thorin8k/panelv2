<?php
class AppUtils{
    public static function createMenuStruct($array){
        $htmlString = "";
        foreach($array as $value){
            //Set tag active
            $active ="";
            if(isset($value['active']) && $value['active']===true){
                $active = "class='active'";
            }
            //Load childrens
            $childrenTag = "";
            $arrowChildren = "";
            $valuesChildren = "";
            if(isset($value['children']) && count($value['children']) !== 0){
                $active = "class='dropdown'";
                $childrenTag = "class='dropdown-toggle' data-toggle='dropdown' ";
                $arrowChildren = " <b class='caret'></b>";
                $valuesChildren .= "<ul class='dropdown-menu'>";
                $valuesChildren .= self::createMenuStruct($value['children']);
                $valuesChildren .= "</ul>";
            }

            $htmlString.="<li ".$active."><a $childrenTag page='".$value['page']."'>";
            $htmlString.= $value['text'].$arrowChildren;
            $htmlString.= "</a>$valuesChildren</li>";
        }
        return $htmlString;
    }
    public static function getLoginForm($withError = false){
        $errorText = "";
        if($withError){
            $errorText = "<p class='navbar-text pull-right text-error'>Usuario o contraseña erroneos. &nbsp;</p>";
        }
        return '<form class="navbar-form pull-right" id="loginForm">
          <input class="span2" type="text" name="login" placeholder="Login" required>
          <input class="span2" type="password" name="password" placeholder="Password" required>
          <button class="btn" type="submit" id="submitLogin">Sign in</button>
        </form>'.$errorText;
    }

    public static function getLoggedText($text){
        $html = '<ul class="nav navbar-nav pull-right" id="loginActions">
                <li class="dropdown logdata">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="./app/statics/img/usr.png" class="imglogin">  '.$text.' <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                      <li class="nav-header">User Data</li>
                      <li><a href="#" page="UserProfile">Perfil</a></li>
                      <li class="divider"></li>

                      <li><a href="index.php?p=site/logout">Salir</a></li>
                    </ul>
                </li>
                </ul>';
        return $html;
    }

    public static function createSimpleArray($data, $key, $value, $withEmpty = false){
        if(empty($data)) return array();
        $resArray = array();
        if($withEmpty){
            $resArray[''] = '';
        }
        foreach($data as $object){
            $resArray[$object->$key] = $object->$value;
        }

        return $resArray;

    }
}
?>
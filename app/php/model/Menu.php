<?php
class Menu  extends ActiveRecord\Model{
    static $table_name = 'menu';
    
    static $has_many = array(
       array('menu_profile' , 'class_name' => 'MenuProfile')
    );
}


<?php
class MenuProfile  extends ActiveRecord\Model{
    static $table_name = 'menu_profile';
    
    static $belongs_to  = array(
       array('profile' , 'class_name' => 'Profile'),
       array('menu' , 'class_name' => 'Menu')
    );
}


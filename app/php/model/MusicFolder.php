<?php
class MusicFolder extends ActiveRecord\Model{
    static $table_name = 'musicfolder';
    
    static $belongs_to = array(
        
        array('user' , 'class_name' => 'User')
    );
}
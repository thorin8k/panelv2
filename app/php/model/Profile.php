<?php
class Profile extends ActiveRecord\Model{
    static $table_name = 'profile';
    
    static $has_many = array(
        array('user' , 'class_name' => 'User'),
        array('menu_profile' , 'class_name' => 'MenuProfile'),
        array('menu', 'through' => 'menu_profile','order' => 'ordercode asc')
    );
}


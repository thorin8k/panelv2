<?php
class User extends ActiveRecord\Model{
    public $folder;
    static $table_name = 'user';
    
    static $has_many = array(
        array('musicfolders', 'class_name' => 'MusicFolder'),
    );
    static $belongs_to = array(
        
        array('profile' , 'class_name' => 'Profile')
    );
}



$(document).ready(function(){		
	$("#loginButton")
 	.click(function() { 
 		login();	 		
 	 });
	
	$("#j_password")
		.keyup(function(l) {
			 if (l.keyCode == 13) {
				 login();
			 }
		});
	
});

function login(){
	var user = $("#j_username").val();
	var pass = $("#j_password").val();
	
	var param ={
			j_username : user,
			j_password :  pass					
	};
	
	$.post("j_spring_security_check", param,
			 function(response){				
				if(response.success){	
					window.location = response.data;
				}else{
					alert(response.error);
				}		
		});	
}
var AdminFolders = ViewPanel.extend({
	path: 'index.php?p=admin/folder',
	renderTo: '#subcontenido',
	afterLoad: function(response){
		var self = this;
	},
	createFolder: function(send){
		var self = this;
		var data = send ? $('#formFolder').serialize() : "";

        application.doAjax(
            "index.php?p=folder/create",
            'post',
            data,
            function(result){
                if(result.form){
                    if(result.success){
                        $('#myModal').html(result.div);
                        $('#myModal #acceptForm').click(function(){
                            if(validateForm($('#formFolder'))){
                                self.createFolder(true)
                            }
                        });
                    }
                }else{
                    if(result.success){
                        $('#myModal').modal('hide');
                        self.load(function(){
                            createNotification('Correcto', result.div, 'success');
                        });
                    }
                }
            }
        );
        $('#myModal').html('<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h3>Cargando...</h3></div>');
        $('#myModal').modal('toggle');
	},
	editFolder: function(elm, send){
		var self = this;
		var data = send ? $('#formFolder').serialize() : "";

        application.doAjax(
            "index.php?p=folder/update&id="+elm.attr('itemid'),
            'post',
            data,
            function(result){
                if(result.form){
                    if(result.success){
                        $('#myModal').html(result.div);
                        $('#myModal #acceptForm').click( function(){
                            if(validateForm($('#formFolder'))){
                                self.editFolder(elm,true);
                            }
                        });
                    }
                }else{
                    if(result.success){
                        $('#myModal').modal('hide');
                        self.load(function(){
                            createNotification('Correcto', result.div, 'success');
                        });
                    }
                }
            }
        );

        $('#myModal').html('<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h3>Cargando...</h3></div>');
        $('#myModal').modal('toggle');
	},
	deleteFolder: function(elm){
		var self = this;
		if(confirm('Esta seguro?')){
            application.doAjax(
                 "index.php?p=folder/delete&id="+elm.attr('itemid'),
                 'post',
                 null,
                 function(result){
                    self.load(function(){
                        createNotification('Correcto', result.div, 'success');
                    });
                }
            );
        }
	}
});
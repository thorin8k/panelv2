var AdminUsers = ViewPanel.extend({
	path: 'index.php?p=admin/user',
	renderTo: '#subcontenido',
	afterLoad: function(response){
		var self = this;
	},
	createUser: function(send){
		var self = this;
		var data = send ? $('#formUser').serialize() : "";

        application.doAjax(
            "index.php?p=user/create",
            'post',
            data,
            function(result){
                if(result.form){
                    if(result.success){
                        $('#myModal').html(result.div);
                        $('#myModal #acceptForm').click(function(){
                            if(validateForm($('#formUser'))){
                                self.createUser(true);
                            }
                        });
                    }
                }else{
                    if(result.success){
                        $('#myModal').modal('hide');
                        self.load(function(result1){
                        	createNotification('Correcto', result.div, 'success');
                        });
                    }
                }
            }
        );
        $('#myModal').html('<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h3>Cargando...</h3></div>');
        $('#myModal').modal('toggle');
	},
	deleteUser: function(elm){
		var self = this;
		if(confirm('Esta seguro?')){
            application.doAjax(
                 "index.php?p=user/delete&id="+elm.attr('itemid'),
                 'post',
                 null,
                 function(result){
                    self.load(function(result1){
                        createNotification('Correcto', result.div, 'success');
                    });
                }
            );
        }
	},
	editUser: function(elm, send){
		var self = this;
		var data = send ? $('#formUser').serialize() : "";

        application.doAjax(
            "index.php?p=user/update&id="+elm.attr('itemid'),
            'post',
            data,
            function(result){
                if(result.form){
                    if(result.success){
                        $('#myModal').html(result.div);
                        $('#myModal #login_field').prop('disabled',true);
                        $('#myModal #acceptForm').click( function(){
                            if(validateForm($('#formUser'))){
                                self.editUser(elm,true);
                            }
                        });
                    }
                }else{
                    if(result.success){
                        $('#myModal').modal('hide');
                        self.load(function(){
                            createNotification('Correcto', result.div, 'success');
                        });
                    }
                }
            }
        );

        $('#myModal').html('<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h3>Cargando...</h3></div>');
        $('#myModal').modal('toggle');

	}

});
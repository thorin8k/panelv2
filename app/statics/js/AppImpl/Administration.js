var Administration = ViewPanel.extend({
	path: 'index.php?p=admin/panel',
	renderTo: '#mainContainer',
	afterLoad: function(response){
		var self = this;
		$('#submenu li a').click(function(){
            $('#submenu li.active').removeClass("active");
            $(this).parent().addClass('active');
            console.log($(this).attr('page'));
            if(typeof $(this).attr('page') !== "undefined" && $(this).attr('page') !== ""){
                //showOnPane('subcontenido',$(this).attr('page'));
                application.changeActiveView($(this).attr('page'));
            }
        });
	}
});
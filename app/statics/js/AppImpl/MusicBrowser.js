var MusicBrowser = ViewPanel.extend({
	path: 'index.php?p=site/music',
	renderTo: '#mainContainer',
	afterLoad: function(response){
		var self = this;
		var elf = $("#elfinder").elfinder({
			url : "index.php?p=site/folder", // connector URL (REQUIRED)
			lang: "es"            // language (OPTIONAL)
		}).elfinder("instance");
        $("#mainContainer").css("width","90%");
	}
});
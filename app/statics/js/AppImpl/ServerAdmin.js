var ServerAdmin = ViewPanel.extend({
	path: 'index.php?p=server/panel',
	renderTo: '#mainContainer',
	afterLoad: function(response){
		var self = this;
		$('#submenu li a').click(function(){
            $('#submenu li.active').removeClass("active");
            $(this).parent().addClass('active');
            if(typeof $(this).attr('page') !== "undefined" && $(this).attr('page') !== ""){
                //showOnPane('subcontenido',$(this).attr('page'));
                application.changeActiveView($(this).attr('page'));
            }
        });
	}
});
var UserProfile = ViewPanel.extend({
	path: 'index.php?p=user/profile',
	renderTo: '#mainContainer',
	afterLoad: function(response){
		var self = this;
	},
	changePwd: function(send){
		var self = this;
		var data = send ? $('#formpwd').serialize() : "";

        application.doAjax(
            "index.php?p=user/changepwd",
            'post',
            data,
            function(result){
                if(result.form){
                    if(result.success){
                        $('#myModal').html(result.div);
                        $('#myModal #acceptForm').click(function(){
                            if(validateForm($('#formUser'))){
                                self.changePwd(true);
                            }
                        });
                    }
                }else{
                    if(result.success){
                        $('#myModal').modal('hide');
                        self.load(function(){
                            createNotification('Correcto', result.div, 'success');
                        });
                    }
                }
            }
        );
        $('#myModal').html('<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h3>Cargando...</h3></div>');
        $('#myModal').modal('toggle');
	}
});
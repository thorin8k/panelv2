var AjaxHandler = Class.extend({
	//constructor
	init: function(){ },
	doAjax: function(url,type, data, callback){
		var self = this;
		return $.ajax({
			type: type,
			data: data!== null ? data : "",
			url: url,
			success: function(response){
				response = $.parseJSON(response);
				if(response.success){
					callback(response);
				}else{
					self.handleError(response);
				}
			},
			error: self.handleError
		});
	},
	handleError: function(errorResponse){
		if(errorResponse.code){
			switch(errorResponse.code){
				case 403:
					createNotification('Error!', result.message, 'error');
					window.location.reload();
					break;
				case 500:
					createNotification('Error!', result.message, 'error');
					break;
			}
		}
	}
});
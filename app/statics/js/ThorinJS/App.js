var App = Class.extend({
	//vista activa
	activeView:null,
	activeViewId: null,
	//vistas disponibles
	views: [],
	mainViews: [],
	tools: null,
	//gestor para llamadas ajax
	ajaxHandler: null,
	//constructor
	init: function(appViews, mainViews){
		this.ajaxHandler = new AjaxHandler();
		this.views = appViews;
		this.mainViews = mainViews;
	},
	run: function(){
		this.loadMainViews();
	},
	loadMainViews: function(){
		var actObj = null;
		for(var i = 0;i < this.mainViews.length;i += 1){
			actObj = this.mainViews[i];
			this.changeActiveView(actObj);
		}
	},
	changeActiveView: function(viewToActivate){

		if(typeof this.views[viewToActivate] !== 'undefined'){
			if(this.activeView && this.activeView.unload) this.activeView.unload();
			this.activeView = this.views[viewToActivate];
			this.activeViewId = viewToActivate;
			this.activeView.load();
			this.onViewChange(this.activeViewId);
		}
	},
	getActiveView: function(){
		return this.activeView;
	},
	onViewChange: function(){},
	doAjax: function(url,type, data, callback){
		return this.ajaxHandler.doAjax(url,type, data, callback);
	},
	callViewMethod: function(viewId,method/*, args */){
		var args = new Array();
	    for (var i = 2; i < arguments.length; i++)
	        args.push(arguments[i]);

		if(this.views[viewId] && this.views[viewId][method]){
			return this.views[viewId][method].apply(this.views[viewId],args);
		}
	},
    getView:function(viewId){
        return this.views[viewId];
    },
	goBack: function(){
		this.activeView.goBack();
	}
});
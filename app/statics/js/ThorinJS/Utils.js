String.prototype.rot13 = function(){
    return this.replace(/[a-zA-Z]/g, function(c){
        return String.fromCharCode((c <= "Z" ? 90 : 122) >= (c = c.charCodeAt(0) + 13) ? c : c - 26);
    });
};
function fnCreateSelect( aData ){
    var r='<select style="width:95%"><option value=""></option>', i, iLen=aData.length;
    for ( i=0 ; i<iLen ; i++ )
    {
        r += '<option value="'+aData[i]+'">'+aData[i]+'</option>';
    }
    return r+'</select>';
}
/**
 * Busca un elemento en un array
 * @param  {[type]} list  [description]
 * @param  {[type]} prop  [description]
 * @param  {[type]} value [description]
 * @return {[type]}       [description]
 */
function searchInObjectArray(list, prop, value){
	for(var idx in list){
		var elm = list[idx];
		if(elm[prop] === value)
			return {idx: idx, obj: elm};
	}
	return null;
}

/**
 * Randomize array element order in-place.
 * Using Fisher-Yates shuffle algorithm.
 */
function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}

function msToTime(duration) {
    var  seconds = parseInt((duration/1000)%60)
        , minutes = parseInt((duration/(1000*60))%60);

    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? "0" + seconds : seconds;

    return minutes + ":" + seconds;
}
var ViewPanel = Class.extend({
	panelId: null,
	renderTo: null,
	path: null,
	title: null,
	backPage: null,
	init: function(){},
	load: function(callback){

		this.ajaxLoad(this.path,callback);
	},
	unload: function(){ 	},
	ajaxLoad: function(path, callback){
		var self = this;
		//TODO mejorar
		$(self.renderTo).html("Cargando...");
		application.doAjax(
			path,
			'get',
			null,
			function(response){
				$(self.renderTo).html(response.div);
				self.afterLoad(response);
				if(callback) callback(response);
			}
		);
	},
	afterLoad: function(){},
	goBack: function(){
		var views = this.backPage;
		if(!$.isArray(this.backPage)){
			views = [this.backPage];
		}

		var actObj = null;
		for(var i = 0;i < views.length;i += 1){
			actObj = views[i];
			application.changeActiveView(actObj);
		}
	}
});
var MusicPlayer = ViewPanel.extend({
	path: 'index.php?p=site/player',
	renderTo: '#mainContainer',
    username: 'thorin',
    password: 'thorin',
	api: null,
	queue: null,
    init: function(){
        this.api = new SubApi("http://thorin.chickenkiller.com:4040", this.username, this.password, "SubChicken");
        this.queue = new PlayerQueue(this.api);
        this.manageEvents();
    },
	afterLoad: function(response){
		var self = this;

		this.api.test(function() {
            self.loadArtists();
        });
	},
    manageEvents: function(){
        var self = this;
        this.queue.player.whileplaying = function(position,duration){
            var percent = (100*position) / duration;

            $('.track-time .current').html(msToTime(position)+"/"+msToTime(duration));
            $('.playback .track-progress-bar').slider('setValue',percent);
        };
        this.queue.onSongChange = function(song){
            $('.playback .play i').addClass('icon-pause').removeClass('icon-play');
            var imagsrc = this.api.getCoverArt(song.id);
            $('.playback .albumArt').attr('src',imagsrc).removeClass('hide');
            $(".playback .songInfo").html('');
            $("#songInfoTmpl").tmpl(song).appendTo(".playback .songInfo");
            $('.nowPlayingList .queuedSong').removeClass('active');
            $('.nowPlayingList .queuedSong[value='+song.id+']').addClass('active');
        };
        this.queue.onPlayStarts = function(){
            if(!$('#playbar').is(':visible')){
                self.showPlayBar();
            }
            $('.playbar-uncollapse i').removeClass('icon-chevron-up').addClass('icon-music');
        };
        this.queue.onPlayStops = function(){
            $('.playbar-uncollapse i').addClass('icon-chevron-up').removeClass('icon-music');
        };
        this.queue.onQueueChange= function(){
            self.showCurrentQueue();
        };
    },
    createEventHandlers: function(){
        var self = this;
        $('body').on('click','.playback .play',function(){
            self.playBtn();
        });
        $('body').on('click','.playback .prev',function(){
            self.prevBtn();
        });
        $('body').on('click','.playback .stop',function(){
            self.stopBtn();
        });
        $('body').on('click','.playback .next',function(){
            self.nextBtn();
        });
        $('body').on('click','.playback .randomize',function(){
            if(self.queue.songList.length === 0) return;
            self.queue.randomize();
            self.queue.startPlay();
        });
        $('body').on('click','.playback .clear',function(){
            if(self.queue.songList.length === 0) return;
            self.queue.clear();
            self.stopBtn();
        });
        $('body').on('click','.playbar-uncollapse, .playbar-collapse',function(){
            if(!$('#playbar').is(':visible')){
                self.showPlayBar();
            }else{
                self.hidePlayBar();
            }
        });
        $('body').on('click','#playlists',function(){
            self.loadPlaylists();
        });
        $('body').on('click','#starred',function(){
            self.loadStarred('song');
        });
        $('body').on('click','.nowPlayingList .queuedSong a',function(){
            self.queue.playById($(this).parent().val());
        });
        $('body').on('click','.nowPlayingList .queuedSong .icon-remove',function(){
            self.queue.removeSong($(this).parent().val());
        });
        $('.playback .track-progress-bar').slider().on('slideStop',function(slideEvt){
            self.goToPosition(slideEvt.value);
        });
        window.addEventListener("keyup", function (eEvent) {
            if(document.activeElement.tagName=='BODY'){
                self.goToLetter(String.fromCharCode(eEvent.keyCode));
            }
        }, false);
    },
	loadArtists: function(){
		var self = this;
		this.api.loadArtists(null, function(list) {
            var data = [];
            for (var i in list) {
                var elem = list[i];
                var elm = { letter: i, artist: elem};
                data.push(elm);
            }
            $("#letterTemplate").tmpl(data).appendTo("#letterList");
            $('.folderList .artistElm').click(function() {
                $('.folderList .active').removeClass('active');
                $(this).addClass('active');
                self.loadAlbums($(this).val());
            });
        });
	},
	loadAlbums: function (id) {
		var self = this;
        this.api.loadAlbumnsByDirectory(id, function(list) {
            $("#albumList").html('');
            $("#albumTemplate").tmpl(list).appendTo("#albumList");
            $('#albumList .albumElm div').click(function() {
                self.loadAlbums($(this).parents(".albumElm").val());
            });
            $('#albumList .albumElm .icon-plus').click(function() {
            	var elm = searchInObjectArray(list, "id", ""+$(this).parents(".albumElm").val()).obj;
                self.addToQueue(elm);
            });$('#albumList .albumElm .icon-play').click(function() {
            	var elm = searchInObjectArray(list, "id", ""+$(this).parents(".albumElm").val()).obj;
                self.playElement(elm);
            });
        });
    },
    loadPlaylists: function(){
    	var self = this;
        this.api.getPlaylists(function(list){
            $("#albumList").html('');
            $("#playlistTemplate").tmpl(list).appendTo("#albumList");
            $('#albumList .playListElm div').click(function() {
                self.loadPlaylistContent($(this).parents(".playListElm").val());
            });
            $('#albumList .playListElm .icon-plus').click(function() {
            	var elm = searchInObjectArray(list, "id", ""+$(this).parents(".playListElm").val()).obj;
                self.addPlaylistToQueue(elm);
            });$('#albumList .playListElm .icon-play').click(function() {
            	var elm = searchInObjectArray(list, "id", ""+$(this).parents(".playListElm").val()).obj
                self.playPlaylist(elm);
            });
        })
    },
    loadPlaylistContent: function(id){
    	var self = this;
        this.api.getPlaylistContent(id, function(list){
        	$("#albumList").html('');
            $("#playlistSongTemplate").tmpl(list).appendTo("#albumList");
            $('#albumList .playListElm .icon-plus').click(function() {
            	var elm = searchInObjectArray(list, "id", ""+$(this).parents(".playListElm").val()).obj;
                self.addToQueue(elm);
            });$('#albumList .playListElm .icon-play').click(function() {
            	var elm = searchInObjectArray(list, "id", ""+$(this).parents(".playListElm").val()).obj;
                self.playElement(elm);
            });
        });
    },
    loadStarred: function(type){
        var self = this;
        this.api.getStarred('100',type,function(list){
            $("#albumList").html('');
            $("#albumTemplate").tmpl(list).appendTo("#albumList");
            $('#albumList .albumElm div').click(function() {
                self.loadPlaylistContent($(this).parents(".albumElm").val());
            });
            $('#albumList .albumElm .icon-plus').click(function() {
                var elm = searchInObjectArray(list, "id", ""+$(this).parents(".albumElm").val()).obj;
                self.addPlaylistToQueue(elm);
            });$('#albumList .albumElm .icon-play').click(function() {
                var elm = searchInObjectArray(list, "id", ""+$(this).parents(".albumElm").val()).obj
                self.playPlaylist(elm);
            });
        })
    },
    addPlaylistToQueue: function(elm, callback){
    	var self = this;
    	this.api.getPlaylistContent(elm.id, function(list){
			var data = [];
            for(var i in list){
                var elem = list[i];
                if(!elem.isDir) self.addToQueue(elem);
            }
            if(callback) callback();
    	});
    },
    playPlaylist: function(elm){
    	var self = this;
    	this.queue.clear();
    	this.addPlaylistToQueue(elm, function(){
    		self.queue.startPlay();
    	});
    },
    addToQueue: function(elm, callback){
    	var self = this;
    	if(!elm.isDir){
    		self.queue.addSong(elm);
    		if(callback) callback();
    	}else{
    		this.api.loadAlbumnsByDirectory(elm.id, function(list) {
    			var data = [];
	            for(var i in list){
	                var elem = list[i];
	                if(!elem.isDir) self.queue.addSong(elem);
	            }
	            if(callback) callback();
    		});
    	}
    },
    playElement: function(elem){
    	var self = this;
    	this.queue.clear();
    	this.addToQueue(elem, function(){
    		self.queue.startPlay();
    	});
    },
    playBtn: function(){
        var btn = $('.playback .play i');
        if(btn.hasClass('icon-pause')){
            btn.removeClass('icon-pause').addClass('icon-play');
        }else{
            btn.addClass('icon-pause').removeClass('icon-play');
        }
    	this.queue.player.togglePause();
    },
    stopBtn: function(){
        $('.playback .track-progress-bar').slider('setValue',0);
        var playBtn = $('.playback .play i');
        playBtn.removeClass('icon-pause').addClass('icon-play');
    	this.queue.player.stop();
    },
    nextBtn: function(){
    	this.queue.playNext();
    },
    prevBtn: function(){
    	this.queue.playPrev();
    },
    goToLetter: function(letter){
    	$('.folderList').scrollTo('#'+letter);
    },
    goToPosition: function(position){
        this.queue.player.setPosition(position);
    },
    showCurrentQueue: function(){
        $('.playback .nowPlayingList').html('');
        $('#queueListTmpl').tmpl(this.queue.songList).appendTo('.playback .nowPlayingList');
    },
    showPlayBar: function(){
        $('#playbar').show("slide", { direction: "down" }, 600);
    },
    hidePlayBar: function(){
        $('#playbar').hide("slide", { direction: "down" }, 600);
    }
});
var Player = Class.extend({
    soundManager: soundManager,
    duration: 0,
    init: function () {
        this.soundManager.debugMode = false;
        this.soundManager.useHTML5Audio = true;
        this.soundManager.url = 'static/js/libs/sm/swf/';
        this.soundManager.preferFlash = false;
    },
    loadSong: function (url, callback) {
        var self = this;
        this.soundManager.onready(function () {
            this.soundManager.destroySound('audio');
            this.soundManager.createSound({
                id: 'audio',
                url: url,
                stream: true,
                type: 'audio/mp3',
                onload: function () {
                    self.onload(this.duration);
                },
                onfinish: function () {
                    self.onfinish();
                },
                whileplaying: function () {
                    self.duration = this.duration;
                    self.whileplaying(this.position, this.duration);
                }
            });
            callback();
        });
    },
    play: function (position, volume) {
        position || (position = 0);
        volume || (volume = 100);

        this.soundManager.play('audio', {
            position: position,
            volume: volume
        });
    },
    stop: function () {
        this.soundManager.stop('audio');
        this.onPlayStops();
    },
    setPosition: function (position) {
        var newPos = (position*this.duration) / 100;
        this.soundManager.setPosition('audio', newPos);
    },
    changeVolume: function (vol) {
        this.soundManager.setVolume('audio', vol);
    },
    toggleMute: function () {
        this.soundManager.toggleMute('audio');
    },
    togglePause: function () {
        this.soundManager.togglePause('audio');
    },
    //Overridables
    onload: function (time) {
    },
    onfinish: function () {
    },
    whileplaying: function (position, duration) {
    },
    onMute: function () {
    },
    onPause: function () {
    },
    onVolumeChanged: function () {
    },
    onPlayStops: function(){

    }
    //End Overridables
});



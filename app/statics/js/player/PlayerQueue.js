var PlayerQueue = Class.extend({
    player: null,
    api: null,
    songList: [],
    currentSongIdx: null,
    init: function (api) {
        var self = this;
        this.player = new Player();
        this.player.onfinish = function () {
            self.playNext();
        };
        this.player.onPlayStops = function(){
            self.onPlayStops();
        };
        this.api = api;
    },
    addSong: function (song) {
        this.songList.push(song);
        this.onQueueChange();
    },
    addFolder: function (songList) {
        for (var sIdx in songList) {
            var song = songList[sIdx];
            this.addSong(song);
        }
    },
    removeSong: function (id) {
        for (var sIdx in this.songList) {
            var song = this.songList[sIdx];
            if (id === song.id) {
                delete this.songList[sIdx];
                this.songList = this.songList.filter(Boolean);
                this.onQueueChange();
                return;
            }
        }
    },
    clear: function () {
        this.songList = [];
        this.onQueueChange();
    },
    randomize: function () {
        this.songList = shuffleArray(this.songList);
        this.onQueueChange();
    },
    playSong: function (index) {
        var self = this;
        this.player.loadSong(this.api.getStreamUrl(this.songList[index].id), function () {
            self.currentSongIdx = index;
            self.player.play();
            self.onSongChange(self.songList[index]);
        });
    },
    startPlay: function () {
        this.playSong(0);
        this.onPlayStarts();
    },
    playNext: function () {
        var newIdx = parseInt(this.currentSongIdx) + 1;
        if (newIdx >= this.songList.length) {
            return;
        }
        this.playSong(newIdx);
    },
    playPrev: function () {
        var newIdx = parseInt(this.currentSongIdx) - 1;
        if (newIdx < 0) {
            return;
        }
        this.playSong(newIdx);
    },
    playById: function(id){
        var pos = searchInObjectArray(this.songList, 'id', id+"").idx;
        this.playSong(pos);
    },
    //Overridables
    onQueueChange: function () {
    },
    onSongChange: function () {
    },
    onPlayStops: function () {
    },
    onPlayStarts: function(){

    }
    //EndOverridables
});
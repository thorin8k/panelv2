var SubApi = Class.extend({
    version: null,
    baseUrl: "",
    username: "",
    password: "",
    appName: "",
    init: function(url, username, password, appName) {
        this.baseUrl = url;
        this.username = username;
        this.password = "enc:" + this.hexEncode(password);
        this.appName = appName;
        this.version = "1.10.2";
    },
    test: function(callback) {
        var self = this;
        $.ajax({
            url: self.baseUrl + '/rest/ping.view?u=' + self.username + '&p=' + self.password + '&v=1.6.0&c=' + self.appName + '&f=jsonp',
            method: 'GET',
            dataType: 'jsonp',
            timeout: 10000,
            success: function(data) {
                if (data["subsonic-response"].status == 'ok') {
                    self.version = data["subsonic-response"].version;
                    callback();
                }
            }
        });
    },
    getStreamUrl: function(songId) {
        var salt = Math.floor(Math.random() * 100000);
        return this.baseUrl + '/rest/stream.view?u=' + this.username + '&p=' + this.password + '&f=jsonp&v=1.10.2&c=' + this.appName + '&id=' + songId + '&salt=' + salt;
    },
    loadArtists: function(folderId, callback) {
        var qry = "";
        if(folderId){
            qry = "&musicFolderId="+folderId;
        }
        var url = this.baseUrl + '/rest/getIndexes.view?u=' + this.username + '&p=' + this.password + '&v=' + this.version + '&c=' + this.appName + '&f=jsonp'+qry;
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'jsonp',
            timeout: 10000,
            success: function(data) {
                var response = [];
                if (data["subsonic-response"].status === 'ok') {
                    var indexes = data["subsonic-response"].indexes.index;
                    if (!$.isArray(indexes)) {
                        indexes = [];
                        indexes[0] = data["subsonic-response"].indexes.index;
                    }
                    for (var i in indexes) {
                        var element = indexes[i];
                        var artistArr = [];
                        for (var j in element.artist) {
                            var artist = element.artist[j];
                            artistArr.push(artist);
                        }
                        response[element.name] = artistArr;
                    }
                    callback(response);
                }
            }
        });
    },
    loadMusicFolders: function(callback) {
        var url = this.baseUrl + '/rest/getMusicFolders.view?u=' + this.username + '&p=' + this.password + '&v=' + this.version + '&c=' + this.appName + '&f=jsonp';
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'jsonp',
            timeout: 10000,
            success: function(data) {
                if (data["subsonic-response"].status === 'ok') {
                    // There is a bug in the API that doesn't return a JSON array for one artist
                    var folders = data["subsonic-response"].musicFolders.musicFolder;
                    if (!$.isArray(folders)) {
                        folders = [];
                        folders[0] = data["subsonic-response"].musicFolders.musicFolder;
                    }
                    callback(folders);
                }
            }
        });
    },
    loadAlbumnsByDirectory: function(dirId,callback) {
        var url = this.baseUrl + '/rest/getMusicDirectory.view?u=' + this.username + '&p=' + this.password + '&v=' + this.version + '&c=' + this.appName + '&f=jsonp&id=' + dirId;
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'jsonp',
            timeout: 10000,
            success: function(data) {
                if (data["subsonic-response"].status === 'ok') {
                    // There is a bug in the API that doesn't return a JSON array for one artist
                    var children = data["subsonic-response"].directory.child;
                    if (!$.isArray(children)) {
                        children = [];
                        if(typeof data["subsonic-response"].directory.child !== 'undefined'){
                            children[0] = data["subsonic-response"].directory.child;
                        }else{
                            children[0] = data["subsonic-response"].directory;
                        }
                    }
                    callback(children);
                }
            }
        });
    },
    /**
     * Type posible values: random, newest, highest, frequent, recent , starred
     *
     * @param {type} type
     * @param {type} size
     * @returns {undefined}
     */
    getAlbumList: function(type, size,callback) {
        var url = this.baseUrl + '/rest/getAlbumList.view?u=' + this.username + '&p=' + this.password + '&v=' + this.version + '&c=' + this.appName + '&f=jsonp&size=' + size + '&type=' + type;
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'jsonp',
            timeout: 10000,
            success: function(data) {
                if (data["subsonic-response"].status === 'ok') {
                    // There is a bug in the API that doesn't return a JSON array for one artist
                    var albums = data["subsonic-response"].albumList.album;
                    if (!$.isArray(albums)) {
                        albums = [];
                        albums[0] = data["subsonic-response"].albumList.album;
                    }
                    callback(albums);
                }
            }
        });
    },
    getRandomSongs: function(size, musicFolderId,callback) {
        var param = "";
        if (musicFolderId) {
            param = '&musicFolderId=' + musicFolderId;
        }
        var url = this.baseUrl + '/rest/getRandomSongs.view?u=' + this.username + '&p=' + this.password + '&v=' + this.version + '&c=' + this.appName + '&f=jsonp&size=' + size + param;
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'jsonp',
            timeout: 10000,
            success: function(data) {
                if (data["subsonic-response"].status === 'ok') {

                    // There is a bug in the API that doesn't return a JSON array for one artist
                    var items = data["subsonic-response"].randomSongs.song;
                    if (!$.isArray(items)) {
                        items = [];
                        items[0] = data["subsonic-response"].randomSongs.song;
                    }
                    callback(items);
                }
            }
        });
    },
    getStarred: function(size, type,callback) {
        var url = this.baseUrl + '/rest/getStarred.view?u=' + this.username + '&p=' + this.password + '&v=' + this.version + '&c=' + this.appName + '&f=jsonp&size=' + size;
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'jsonp',
            timeout: 10000,
            success: function(data) {
                if (data["subsonic-response"].status === 'ok') {
                    // There is a bug in the API that doesn't return a JSON array for one artist
                    var items = [];
                    switch (type) {
                        case 'artist':
                            if (data["subsonic-response"].starred.artist !== undefined) {
                                items = data["subsonic-response"].starred.artist;
                                if (!$.isArray(items)) {
                                    items = [];
                                    items[0] = data["subsonic-response"].starred.artist;
                                }
                            }
                            break;
                        case 'album':
                            if (data["subsonic-response"].starred.album !== undefined) {
                                items = data["subsonic-response"].starred.album;
                                if (!$.isArray(items)) {
                                    items = [];
                                    items[0] = data["subsonic-response"].starred.album;
                                }
                            }
                            break;
                        case 'song':
                            if (data["subsonic-response"].starred.song !== undefined) {
                                items = data["subsonic-response"].starred.song;
                                if (!$.isArray(items)) {
                                    items = [];
                                    items[0] = data["subsonic-response"].starred.song;
                                }
                            }
                            break;
                    }
                    callback(items);

                }
            }
        });
    },
    getNowPlaying: function(callback) {
        var url = this.baseUrl + '/rest/getNowPlaying.view?u=' + this.username + '&p=' + this.password + '&v=' + this.version + '&c=' + this.appName + '&f=jsonp';
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'jsonp',
            timeout: 10000,
            success: function(data) {
                if (data["subsonic-response"].status === 'ok' && data["subsonic-response"].nowPlaying.entry !== undefined) {

                    var msgs = data["subsonic-response"].nowPlaying.entry;
                    if (!$.isArray(msgs)) {
                        msgs = [];
                        msgs[0] = data["subsonic-response"].nowPlaying.entry;
                    }
                    callback(msgs);
                }
            }
        });
    },
    search: function(query,callback) {
        var url = this.baseUrl + '/rest/search2.view?u=' + this.username + '&p=' + this.password + '&v=' + this.version + '&c=' + this.appName + '&f=jsonp&query=' + query;
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'jsonp',
            timeout: 10000,
            success: function(data) {
                if (data["subsonic-response"].status === 'ok' && data["subsonic-response"].searchResult2 !== "") {

                    var children = [];
                    if (data["subsonic-response"].searchResult2.song !== undefined) {
                        children['songs'] = data["subsonic-response"].searchResult2.song;
                        if (!$.isArray(children['songs'])) {
                            children['songs'] = [];
                            children['songs'][0] = data["subsonic-response"].searchResult2.song;
                        }
                    }
                    if (data["subsonic-response"].searchResult2.album !== undefined) {
                        children['albums'] = data["subsonic-response"].searchResult2.album;
                        if (!$.isArray(children['albums'])) {
                            children['albums'] = [];
                            children['albums'][0] = data["subsonic-response"].searchResult2.album;
                        }
                    }

                    callback(children)
                }
            }
        });
    },
    getPlaylists: function(callback) {
        var url = this.baseUrl + '/rest/getPlaylists.view?u=' + this.username + '&p=' + this.password + '&v=' + this.version + '&c=' + this.appName + '&f=jsonp';
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'jsonp',
            timeout: 10000,
            success: function(data) {
                if (data["subsonic-response"].status === 'ok') {
                    var playlists = data["subsonic-response"].playlists.playlist;
                    if (!$.isArray(playlists)) {
                        playlists = [];
                        playlists[0] = data["subsonic-response"].playlists.playlist;
                    }

                    callback(playlists);
                }
            }
        });
    },
    createPlaylist: function(name,callback) {
        var url = this.baseUrl + '/rest/createPlaylist.view?u=' + this.username + '&p=' + this.password + '&v=' + this.version + '&c=' + this.appName + '&f=jsonp&name=' + name;
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'jsonp',
            timeout: 10000,
            success: function(data) {
                callback(data);
            }
        });
    },
    deletePlaylist: function(id,callback) {
        var url = this.baseUrl + '/rest/deletePlaylist.view?u=' + this.username + '&p=' + this.password + '&v=' + this.version + '&c=' + this.appName + '&f=jsonp&id=' + id;
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'jsonp',
            timeout: 10000,
            success: function(data) {
                callback(data);
            }
        });
    },
    updatePlaylist: function(playListId, songsToAdd,callback) {
        var url = this.baseUrl + '/rest/updatePlaylist.view?u=' + this.username + '&p=' + this.password;
        $.ajax({
            type: 'GET',
            url: url,
            dataType: 'jsonp',
            timeout: 10000,
            data: {v: this.version, c: this.appName, f: "jsonp", playlistId: playListId, songIdToAdd: songsToAdd},
            success: function(data) {
                callback(data);
            },
            traditional: true // Fixes POST with an array in JQuery 1.4
        });
    },
    getPlaylistContent: function(id,callback) {
        var url = this.baseUrl + '/rest/getPlaylist.view?u=' + this.username + '&p=' + this.password + '&v=' + this.version + '&c=' + this.appName + '&f=jsonp&id=' + id;
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'jsonp',
            timeout: 10000,
            success: function(data) {
                if (data["subsonic-response"].status === 'ok' && data["subsonic-response"].playlist.entry !== undefined) {

                    // There is a bug in the API that doesn't return a JSON array for one artist
                    var children = [];
                    var playlist = data["subsonic-response"].playlist;
                    if (playlist.entry.length > 0) {
                        children = playlist.entry;
                    } else {
                        children[0] = playlist.entry;
                    }
                    callback(children);
                }
            }
        });
    },
    getShares: function(callback) {
        var url = this.baseUrl + '/rest/getShares.view?u=' + this.username + '&p=' + this.password + '&v=' + this.version + '&c=' + this.appName + '&f=jsonp';
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'jsonp',
            timeout: 10000,
            success: function(data) {
                if (data["subsonic-response"].status === 'ok') {

                    var shares = data["subsonic-response"].shares.share;
                    if (!$.isArray(shares)) {
                        shares = [];
                        shares[0] = data["subsonic-response"].shares.share;
                    }
                    callback(shares);
                }
            }
        });
    },
    createShare: function(id, callback) {
        var url = this.baseUrl + '/rest/createShare.view?u=' + this.username + '&p=' + this.password + '&v=' + this.version + '&c=' + this.appName + '&f=jsonp&id=' + id;
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'jsonp',
            timeout: 10000,
            success: function(data) {
                if (data["subsonic-response"].status === 'ok') {

                    var shares = data["subsonic-response"].shares.share;
                    if (!$.isArray(shares)) {
                        shares = [];
                        shares[0] = data["subsonic-response"].shares.share;
                    }
                    callback(shares);
                }
            }
        });
    },
    deleteShare: function(id, callback) {
        var url = this.baseUrl + '/rest/deleteShare.view?u=' + this.username + '&p=' + this.password + '&v=' + this.version + '&c=' + this.appName + '&f=jsonp&id=' + id;
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'jsonp',
            timeout: 10000,
            success: function(data) {
                if (data["subsonic-response"].status === 'ok') {

                    callback(data);
                }
            }
        });
    },
    star: function(id,callback) {
        var url = this.baseUrl + '/rest/star.view?u=' + this.username + '&p=' + this.password + '&v=' + this.version + '&c=' + this.appName + '&f=jsonp&id=' + id;
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'jsonp',
            timeout: 10000,
            success: function(data) {
                if (data["subsonic-response"].status === 'ok') {

                    callback(data);
                }
            }
        });
    },
    unstar: function(id, callback) {
        var url = this.baseUrl + '/rest/unstar.view?u=' + this.username + '&p=' + this.password + '&v=' + this.version + '&c=' + this.appName + '&f=jsonp&id=' + id;
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'jsonp',
            timeout: 10000,
            success: function(data) {
                if (data["subsonic-response"].status === 'ok') {

                    callback(data);
                }
            }
        });
    },
    getDownloadUrl: function(id) {
        var url = this.baseUrl + '/rest/download.view?u=' + this.username + '&p=' + this.password + '&v=' + this.version + '&c=' + this.appName + '&f=jsonp&id=' + id;
        return url;
    },
    getCoverArt: function(id) {
        var url = this.baseUrl + '/rest/getCoverArt.view?v=' + this.version + '&c=' + this.appName + '&f=jsonp&size=50&id=' + id;
        return url;
    },
    getChatMessages: function(callback){
        var url = this.baseUrl + '/rest/getChatMessages.view?u=' + this.username + '&p=' + this.password + '&v=' + this.version + '&c=' + this.appName + '&f=jsonp';
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'jsonp',
            timeout: 10000,
            success: function(data) {
                if (data["subsonic-response"].status === 'ok') {

                    callback(data);
                }
            }
        });
    },
    addChatMessage: function(message, callback){
        var url = this.baseUrl + '/rest/addChatMessage.view?u=' + this.username + '&p=' + this.password + '&v=' + this.version + '&c=' + this.appName + '&f=jsonp';
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'jsonp',
            timeout: 10000,
            data: {message: message},
            success: function(data) {
                if (data["subsonic-response"].status === 'ok') {

                    callback(data);
                }
            }
        });
    },
    getAvatar: function(user){
        var url = this.baseUrl + '/rest/getAvatar.view?u=' + this.username + '&p=' + this.password + '&v=' + this.version + '&c=' + this.appName + '&f=jsonp&username='+user;
        return url;
    },
    hexEncode: function(n) {
        for (var u = "0123456789abcdef", i = [], r = [], t = 0; t < 256; t++)
            i[t] = u.charAt(t >> 4) + u.charAt(t & 15);
        for (t = 0; t < n.length; t++)
            r[t] = i[n.charCodeAt(t)];
        return r.join("")
    }
});



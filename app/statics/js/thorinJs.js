var views = {
    'MusicBrowser': new MusicBrowser(),
    'Home': new Home(),
    'MusicPlayer': new MusicPlayer(),
    'ServerAdmin': new ServerAdmin(),
    'Administration': new Administration(),
    'AdminUsers': new AdminUsers(),
    'AdminFolders': new AdminFolders(),
    'ServerInfo': new ServerInfo(),
    'NasInfo': new NasInfo(),
    'UserProfile': new UserProfile()
};
var application;
//Menu navigation
$(document).ready(function () {
    application = new App(views, []);
    application.run();
    application.onViewChange = function (currView) {
        if ($('ul.nav li a[page="' + currView + '"]').parent().hasClass('logdata')) return;
        $('ul.nav li.active').removeClass("active");
        $('ul.nav li a[page="' + currView + '"]').parent().addClass('active');
    }
    $('body').on('click', '#menuList li a', function () {
        application.changeActiveView($(this).attr('page'));
    });

    $('body').on('click','#loginActions li a',function () {
        application.changeActiveView($(this).attr('page'));
    });

    $('body').on('submit','#loginForm', function (ev) {
        submitLoginForm(ev);
    })
});
function submitLoginForm(ev) {

    $.ajax({
        url: "index.php?p=site/login",
        data: $("#loginForm").serialize(),
        type: 'post',
        success: function (response) {
            var result = $.parseJSON(response);
            $('.login-data').html(result.div);
            if(result.reload === true){
                window.location.reload();
            }
            //reloadMenu();
        }
    })
    ev.preventDefault();
}

/*function reloadMenu() {
    $('#menuList').html("<p class='navbar-text text-error'>Loading...</p>");
    $.ajax({
        url: "index.php?p=site/reloadMenu",
        type: 'post',
        success: function (response) {
            var result = $.parseJSON(response);
            $('#menuList').html(result.div);
        }
    });
}*/

function validateForm(form) {
    var isValid = true;
    $('.modal .error-summary').html('');
    $('#' + form.attr('id') + " :input").each(function (index, elm) {
        $(this).parent().removeClass('error');
        if ($(this).attr('req')) {
            var value = $(this).val();
            if (value === null || value === '' || typeof value === 'undefined') {
                $(this).parent().addClass('error');
                isValid = false;
            }
        }
        //TODO Validate regexps?
    });
    if (!isValid) {
        $('.modal .error-summary').html('Existen errores en el formulario.');
    }
    return isValid;
}

function createNotification(title, text, type) {
    $('#notification-bar').html('<div class="alert alert-' + type + '"> ' +
        '<button type="button" class="close" data-dismiss="alert">&times;</button> ' +
        '<strong>' + title + '</strong> ' + text +
        '</div>');
    setTimeout(function () {
        $('#notification-bar').children().fadeOut();
    }, 1600);
}

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
  <h3>Usuarios</h3>
</div>
<div class="modal-body">
  <?php 
    $allUsers = User::all();
    
    echo HtmlGenerator::createForm(
        'formFolder',
        '',
        $model,
        array(
            array('title'=>'Nombre', 'attr'=>'name', 'required'=> true),
            array('title'=>'Ruta', 'attr'=>'path', 'required'=> true),
            array('title'=>'User', 'attr'=>'user_id', 'type'=>'dropdown', 'data'=>  AppUtils::createSimpleArray($allUsers, 'id', 'login',true), 'required'=> true) ,
        )
    );
  ?>
</div>
<div class="modal-footer">
    <div class="error-summary"></div>
    <button id='acceptForm' class="btn btn-primary">Aceptar</button>
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
</div>

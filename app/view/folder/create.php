<?php
$dao = new FolderDao;

echo HtmlGenerator::createButton("btnAddFolder", "btn btn-small btn-primary", "Añadir Carpeta", 'application.getActiveView().createFolder();');
echo "<br/><br/>";
echo HtmlGenerator::createTable(
        "tableTest",//TableId
        $dao->getAll(),//Data
        array( //Columns to show
            array('field'=>'name','header'=>"Nombre"),
            array('field'=>'path','header'=>"Ruta"),
            array('field'=>'relation:user-login','header'=>"Usuario"),
        ),
        array( //Options
            'columns'=>array(
                'edit'=> array('icon'=>'icon-pencil','callback'=>'application.getActiveView().editFolder($(this))','title'=>'Modificar'),
                'delete'=>array('icon'=>'icon-remove','title'=>'Eliminar','callback'=>'application.getActiveView().deleteFolder($(this));')
            )
        )
    );

?>

<div id="myModal" class="modal hide">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Cargando...</h3>
    </div>
</div>
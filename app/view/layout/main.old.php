<!DOCTYPE html>
<html lang="es">
  <head>
    <!-- Le styles -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php 
        App::registerStyle("app/statics/js/ext/resources/ext-theme-landra/ext-theme-landra-all-debug.css");
        App::computeStyles();
        
        App::registerScript("app/statics/js/ext/ext-all.js");
        App::registerScript("app/statics/js/ext/locale/ext-lang-es.js");
        App::registerScript("app/statics/js/ext/resources/plugins/pagingResizer/Ext.ux.plugin.PagingToolbarResizer.js");
        App::registerScript("app/statics/js/utils.js");
        App::computeScripts();
    ?>
    <meta charset="UTF-8">
    <title><?php echo App::getConfigValue('name');?></title>
  </head>
 <body>
      <div class="main-container"> 
          <div class="breadcrumb-container">
              <ul class="breadcrumb-list">
                  <li class="breadcrumb "><?php echo HtmlGenerator::createLink(App::getUrlToPage('patient/extern&pid1=123456',true), 'External Ref.'); ?></li>
                  <li class="breadcrumb active"><?php echo HtmlGenerator::createLink(App::getUrlToPage('patient/index'), 'Búsqueda Paciente'); ?></li>
              </ul>
              <div class="x-clear"></div>
          </div>
          <a class='login-info' href="<?php echo App::getUrlToPage('site/logout');?>" >Salir</a>
        <div class="submain-container">
          
          <!-- Main Content--> 
          <?php 
            echo $mainContent;
          ?>
        </div>
      </div>
     <div class="footer">
        <img alt="" src="app/statics/images/roche.png" height="30" width="60">
     </div>
     <?php 
        App::registerScript("app/statics/js/ext/resources/ext-theme-landra/style-tools.js");
        App::computeScripts();
     ?>
</body>
</html>


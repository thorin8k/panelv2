<?php

//Default Menu
$defMenu = array(
    array('text' => 'Home', 'page' => 'Home', 'active' => true),
);
//User Menu
$arrayMenu = array_merge($defMenu, App::getSession()->getUserMenu());
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <!-- Le styles -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php
    App::registerScript("app/statics/ext/jquery/jquery.js");
    App::registerScript("app/statics/ext/jquery/jquery-migrate-1.1.1.js");

    App::registerScript("app/statics/ext/bootstrap/js/bootstrap.min.js");
    App::registerScript("app/statics/ext/jquery/jquery-ui.js");
    App::registerScript("app/statics/ext/jquery/jquery.tmpl.min.js");
    App::registerScript("app/statics/ext/jquery/jquery.dataTables.min.js");
    App::registerScript("app/statics/ext/jquery/paging.js");
    App::registerScript("app/statics/ext/jquery/jquery.scrollTo.js");
    App::registerScript("app/statics/ext/jquery/bootstrap-slider.js");
    App::registerScript("app/statics/ext/jquery/custom.datatable.filter.js");
    App::registerScript("app/statics/ext/elfinder/js/elfinder.min.js");
    App::registerScript("app/statics/ext/elfinder/js/i18n/elfinder.es.js");
    App::registerScript("app/statics/js/player/libs/sm/soundmanager2-jsmin.js");
    App::registerScript("app/statics/js/ThorinJS/libs/Class.js");
    App::registerScript("app/statics/js/player/SubApi.js");
    App::registerScript("app/statics/js/player/Player.js");
    App::registerScript("app/statics/js/player/PlayerQueue.js");
    App::registerScript("app/statics/js/ThorinJS/App.js");
    App::registerScript("app/statics/js/ThorinJS/AjaxHandler.js");
    App::registerScript("app/statics/js/ThorinJS/Utils.js");
    App::registerScript("app/statics/js/ThorinJS/ViewPanel.js");
    App::registerScript("app/statics/js/AppImpl/MusicBrowser.js");
    App::registerScript("app/statics/js/AppImpl/Home.js");
    App::registerScript("app/statics/js/AppImpl/Administration.js");
    App::registerScript("app/statics/js/player/MusicPlayer.js");
    App::registerScript("app/statics/js/AppImpl/ServerAdmin.js");
    App::registerScript("app/statics/js/AppImpl/AdminUsers.js");
    App::registerScript("app/statics/js/AppImpl/AdminFolders.js");
    App::registerScript("app/statics/js/AppImpl/ServerInfo.js");
    App::registerScript("app/statics/js/AppImpl/NasInfo.js");
    App::registerScript("app/statics/js/AppImpl/UserProfile.js");
    App::registerScript("app/statics/js/thorinJs.js");
    App::computeScripts();
    App::registerStyle("app/statics/ext/bootstrap/css/bootstrap.css");
    App::registerStyle("app/statics/css/thorinCss.css");
    App::registerStyle("app/statics/css/player.css");
    App::registerStyle("app/statics/css/jquery-ui.css");
    App::registerStyle("app/statics/css/slider.css");
    App::registerStyle("app/statics/ext/elfinder/css/elfinder.min.css");
    App::registerStyle("app/statics/ext/elfinder/css/theme.css");
    App::computeStyles();
    ?>
    <meta charset="UTF-8">
    <title><?php echo App::getConfigValue('name'); ?></title>
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container-fluid">
            <a class="brand"><?php echo App::getConfigValue('name'); ?> </a>

            <div class="nav-collapse collapse">
                <ul class="nav" id="menuList">
                    <?php
                    echo AppUtils::createMenuStruct($arrayMenu);

                    ?>
                </ul>
                <?php
                if (!App::getSession()->isGuestSession()) {
                    echo AppUtils::getLoggedText(App::getSession()->getUserName());
                }

                ?>
                <div class="login-data pull-right">
                    <?php
                    if (App::getSession()->isGuestSession()) {
                        echo AppUtils::getLoginForm();
                    }

                    ?>
                    <div class='clearfix'></div>
                </div>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row-fluid" style="min-height:500px">
        <div id='notification-bar' class='span6' style='position:absolute;top:50px;right:40px;'></div>
        <div class="span11" id="mainContainer">
            <!-- Main Content-->
            <?php
            echo $mainContent;
            ?>
        </div>
    </div>

    <hr/>
        <?php
            if(!App::getSession()->isGuestSession()){
                $subUser = App::getSession()->getActualUser()->login;
                $userDao = new UserDao();
                $subPwd = $userDao->getUser($subUser)->password;
                echo $controller->loadPage('music/controls_panel',array('subsonicUser' => $subUser, 'subsonicPwd'=>$subPwd),true);
            }
        ?>
    <footer>
        <p></p>
    </footer>

</div>
<!--/.fluid-container-->
</body>
</html>

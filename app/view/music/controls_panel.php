<script>
    $(document).ready(function(){
        $('.playback .nowPlayingBtn').popover({
            html: true,
            placement: 'top',
            content: function() {
                return $('.playback .nowPlayingContent').html();
            },
            title: function() {
                return $('.playback .nowPlayingTitle').html();
            }
        });
        application.getView('MusicPlayer').username = "<?php echo $subsonicUser; ?>";
        application.getView('MusicPlayer').password = "<?php echo $subsonicPwd; ?>";
        application.getView('MusicPlayer').createEventHandlers();
    })
</script>
<div class="playbar-btn playbar-uncollapse">
    <button class='btn'><i class='icon-chevron-up'></i></button>
</div>
<div id="playbar" class="navbar  navbar-inverse navbar-fixed-bottom hide">
    <div class="navbar-inner">
        <div class="container-fluid">

            <div class='playback'>
                <div class='btn-group span2' style="width:auto;">
                    <button class='prev btn'><i class='icon-step-backward'></i></button>
                    <button class='play btn'><i class='icon-play'></i></button>
                    <button class='stop btn'><i class='icon-stop'></i></button>
                    <button class='next btn'><i class='icon-step-forward'></i></button>
                </div>
                <div class='span3'>
                    <a class="pull-left" href="#" style="margin-top: 1px;">
                        <img class='albumArt img-polaroid media-object hide' style="width:27px;"/>
                    </a>

                    <div class='songInfo media-body'></div>
                </div>
                <div class="btn-group span1 " style="width:auto;">
                    <button class="btn nowPlayingBtn">
                        <i class="icon-list"></i>
                    </button>
                    <button class="btn randomize" title="Randomize">
                        <i class="icon-random"></i>
                    </button>
                    <button class="btn clear" title="Clear">
                        <i class="icon-ban-circle"></i>
                    </button>
                </div>
                <div class="nowPlaying hide">
                    <div class="nowPlayingTitle">
                        <span>Queue</span>
                    </div>
                    <did class="nowPlayingContent">
                        <ul class="nowPlayingList"></ul>
                    </did>

                </div>
                <div class="track-time span2">
                    <small>
                        <span class="current"></span>
                    </small>
                </div>
                <div class='span4' style="margin-top: 8px;">
                    <div class="track-progress">
                        <input class='track-progress-bar slider' type="text" data-slider-min="0"
                               data-slider-max="100" value="0" data-slider-step="1" data-highlight='true'
                               data-slider-tooltip="hide"/>
                    </div>
                </div>
                <div class="playbar-collapse pull-right">
                    <button class='btn'><i class='icon-chevron-down'></i></button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Templates -->
<script id="artistTemplate" type="text/html">
    <ul class="bs-docs-sidenav">{{each artist}}<li class='artistElm' value='${id}'><a><div title="${name}">${name}</div><i class="icon-th-list"></i></a></li>{{/each}}</ul>
</script>
<script id="letterTemplate" type="text/html">
    <li><a id="${letter}">${letter}</a></li>
    {{tmpl($data) "#artistTemplate"}}
</script>
<script id="albumTemplate" type="text/html">
    <li class='albumElm' value='${id}'>
        <a>
            <div title="${title}">${title}</div>
            <i class="icon-play"></i>
            <i class="icon-plus"></i>
            {{if isDir == true}}
                <i class="icon-folder-close"></i>
            {{/if}}
        </a>
    </li>
</script>
<script id="playlistTemplate" type="text/html">
    <li class='playListElm' value='${id}'>
        <a>
            <div title="${name}">${name}</div>
            <i class="icon-play"></i>
            <i class="icon-plus"></i>
            <i class="icon-folder-close"></i>
        </a>
    </li>
</script>
<script id="playlistSongTemplate" type="text/html">
    <li class='playListElm' value='${id}'>
        <a>
            <div title="${title}">${title}</div>
            <i class="icon-play"></i>
            <i class="icon-plus"></i>
            <i class="icon-folder-close"></i>
        </a>
    </li>
</script>
<script id="songInfoTmpl" type="text/html">
    Artist: ${artist}<br/> Title: ${title}
</script>
<script id="queueListTmpl" type="text/html">
    <li class="queuedSong" value="${id}">
        <i class="icon-remove"></i>
        <a>${title}</a>
    </li>
</script>
<!-- End Templates -->
<div>
    <div class="span3">
        <div class="span12">
        <ul class='nav nav-list bs-docs-sidenav'>
            <li class='artistElm playlists' id='playlists' value='playlists'><a><div title="Playlists">Playlists</div><i class="icon-th-list"></i></a></li>
            <li class='artistElm' id='starred' value='starred'><a><div title="Starred">Starred</div><i class="icon-th-list"></i></a></li>
        </ul>
        </div>
        <div class="folderList span12">
            <ul id='letterList' class='nav nav-list bs-docs-sidenav'></ul>
        </div>
    </div>
    <div class="span8">
        <div class="rightPanel ">
            <ul id='albumList' class='nav nav-list bs-docs-sidenav'></ul>
        </div>
    </div>
</div>
<div class="clear"></div>

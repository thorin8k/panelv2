
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
  <h3>Usuarios</h3>
</div>
<div class="modal-body">
  <?php 
    $allprofiles = Profile::all();
    $folder=array();
    if($model->is_new_record()){
        $folder = array('title'=>'Folder Path', 'attr'=>'folder', 'required'=> false );
    }
    echo HtmlGenerator::createForm(
        'formUser',
        '',
        $model,
        array(
            array('title'=>'Usuario', 'attr'=>'login', 'required'=> true),
            array('title'=>'Contraseña', 'attr'=>'password', 'type'=>'password', 'required'=> true ),
            array('title'=>'Perfil', 'attr'=>'profile_id', 'type'=>'dropdown', 'data'=>  AppUtils::createSimpleArray($allprofiles, 'id', 'name',true), 'required'=> true),
            $folder
        )
    );
  ?>
</div>
<div class="modal-footer">
    <div class="error-summary"></div>
    <button id='acceptForm' class="btn btn-primary">Aceptar</button>
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
</div>

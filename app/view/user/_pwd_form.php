<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3>Cambiar Contraseña</h3>
        </div>
        <div class="modal-body">
          <?php

            echo HtmlGenerator::createForm(
                'formpwd',
                '',
                $model,
                array(
                    array('title'=>'Contraseña', 'attr'=>'password', 'type'=>'password', 'required'=> true ),
                )
            );
          ?>
        </div>
        </div>
        <div class="modal-footer">
            <div class="error-summary"></div>
            <button id='acceptForm' class="btn btn-primary">Aceptar</button>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
        </div>
    </div>
</div>

<?php
$dao = new UserDao;

echo HtmlGenerator::createButton("btnAddUser", "btn btn-small btn-primary", "Añadir Usuario", 'application.getActiveView().createUser();');
echo "<br/><br/>";
echo HtmlGenerator::createTable(
        "tableTest",//TableId
        $dao->getAll(),//Data
        array( //Columns to show
            array('field'=>'login','header'=>"Nombre Usuario"),
            array('field'=>'relation:profile-name','header'=>"Perfil"),
        ),
        array( //Options
            'columns'=>array(
                'edit'=> array('icon'=>'icon-pencil','callback'=>'application.getActiveView().editUser($(this))','title'=>'Modificar'),
                'delete'=>array('icon'=>'icon-remove','title'=>'Eliminar','callback'=>'application.getActiveView().deleteUser($(this));')
            )
        )
    );

?>

<div id="myModal" class="modal hide">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Cargando...</h3>
    </div>
</div>
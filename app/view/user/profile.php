<div class="row-fluid">
    <h2>User Profile</h2>
    <div class="span6">
        <div class="control-group info">
            <label class="control-label" for="inputInfo">Nombre de usuario</label>
            <div class="controls">
                <input type="text" id="" disabled="disabled" value='<?php echo $model->login; ?>'>
            </div>
        </div>
        <div class="control-group info">
            <label class="control-label" for="inputInfo">Contraseña</label>
            <div class="controls">
                <input type="password" id="" disabled="disabled" value='***********'>
                <?php echo HtmlGenerator::createButton("btnChgpwd", "btn btn-mini", "Cambiar", 'application.getActiveView().changePwd();');?>
            </div>
        </div>
        <div class="control-group info">
            <label class="control-label" for="inputInfo">Perfil</label>
            <div class="controls">
                <input type="text" id="" disabled="disabled" value='<?php echo $model->profile->name; ?>'>
            </div>
        </div>
        <div class="control-group info">
            <label class="control-label" for="inputInfo">Carpetas</label>
            <?php
                $folders = $model->musicfolders;
                foreach($folders as $folder){
                    echo '<input type="text" id="" disabled="disabled" value="'.$folder->name.'('.$folder->path.')">';
                }
            ?>
            </div>
</div>

<div id="myModal" class="modal hide">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Cargando...</h3>
    </div>
</div>
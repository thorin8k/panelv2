<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);

include_once "app/php/framework/App.php";
//$start = FrameworkUtils::RetrieveTime();

App::init();

App::run();

//$end = FrameworkUtils::RetrieveTime();
//echo FrameworkUtils::CalculateLoadTime($start, $end)." Using: ".FrameworkUtils::MemoryUsage()." of Memory.";
